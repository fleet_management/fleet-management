package com.roshan.bean;

public class Success {
	
	private int id;
	private String type;
	private String orgid;
	private String userName;
	private String UserID;

	
	public Success(int id, String type, String orgid, String userName, String userID) {
		super();
		this.id = id;
		this.type = type;
		this.orgid = orgid;
		this.userName = userName;
		UserID = userID;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getOrgid() {
		return orgid;
	}


	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserID() {
		return UserID;
	}


	public void setUserID(String userID) {
		UserID = userID;
	}
	
}
