package com.roshan.bean;

public class LoginError {

		private String userId;

		public LoginError(String userId) {
			super();
			this.userId = userId;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}
		
}
