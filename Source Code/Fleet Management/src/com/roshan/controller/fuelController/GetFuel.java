package com.roshan.controller.fuelController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblFuleType;
import com.roshan.model.FuelModel;

/**
 * Servlet implementation class GetFuel
 */
@WebServlet("/pages/maintain/GetFuel")
public class GetFuel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetFuel() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Map<String,Object> map = new HashMap<String,Object>();
		int id=0;
		
		try {
		id  = Integer.parseInt(request.getParameter("id"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		

		try{
		FuelModel fm = new FuelModel();
		TblFuleType tf = fm.viewEmployee(id);
		map.put("name", tf.getName());
		map.put("price", tf.getPrice());
		
		write(response,map);
		
		}catch (Exception e) {
			//System.out.println(e);
		}
	
	
	}
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
