package com.roshan.controller.fuelController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblFuleType;
import com.roshan.model.FuelModel;

/**
 * Servlet implementation class DeleteFuel
 */
@WebServlet("/pages/maintain/DeleteFuel")
public class DeleteFuel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteFuel() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		FuelModel fm = new FuelModel();
	
		int id = Integer.parseInt(request.getParameter("id"));
		fm.deleteFuel(id);
		
		
		
	}

}
