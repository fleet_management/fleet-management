package com.roshan.controller.fuelController;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblFulePriceHistoty;
import com.roshan.entity.TblFulePriceHistotyId;
import com.roshan.entity.TblFuleType;
import com.roshan.ids.Ids;
import com.roshan.model.FuelModel;

/**
 * Servlet implementation class UpdateFuel
 */
@WebServlet("/pages/maintain/UpdateFuel")
public class UpdateFuel extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	FuelModel fm = new FuelModel();
	TblFuleType ft = new TblFuleType();
	TblFulePriceHistoty fh = new TblFulePriceHistoty();
	TblFulePriceHistotyId fpi = new TblFulePriceHistotyId();
	Date date = new Date();
    public UpdateFuel() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		String name = request.getParameter("fuel_name");
		boolean result = fm.fuelUpdateValidate(name);
		Map<String,Object> map = new HashMap<String,Object>();
		
		try{
		double price = Double.parseDouble(request.getParameter("price"));
		int id = Integer.parseInt(request.getParameter("id"));
		map.put("isValid", result);
					
		if(!result){
			
			//int x = Ids.getId();
			ft.setId(id);
			ft.setName(name);
			ft.setPrice(price);
			ft.setStatus((byte)1);
			
			
			fpi.setTblFuleTypeId(id);
			int y = Ids.getId()+1;
			fpi.setId(y);
			
			fh.setPrice(price);
			fh.setDate(date);
			fh.setTblFuleType(ft);
			fh.setId(fpi);
			ft.setStatus((byte)1);
			
			fm.UpdateFuel(ft,fh);
			
			
		}
		
		}catch(Exception e){
			System.out.println(e);
		}
		write(response,map);
		
	}

private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

	
}
