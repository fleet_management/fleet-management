package com.roshan.controller.gpsController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblEmployee;
import com.roshan.entity.TblGpsTracking;
import com.roshan.entity.TblSchedule;
import com.roshan.entity.TblVehicle;
import com.roshan.model.GpsModel;

/**
 * Servlet implementation class RealtimeTracking
 */
@WebServlet("/pages/gps/RealtimeTracking")
public class RealtimeTracking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RealtimeTracking() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		System.out.println(id);
		
		GpsModel gm = new GpsModel();
		
		TblVehicle tv = new TblVehicle();
		tv.setId(id);
		List<?>  list = gm.getdata(tv);
		
		System.out.println(list.size());
		
		//for(int j=0; j<list.size(); j++) {		
			//Object[] row = (Object[]) list.get(j);	
			TblGpsTracking tt = (TblGpsTracking)list.get(0);
			
			System.out.println(tt.getLatitude());
			map.put("lat", tt.getLatitude());
			map.put("lon", tt.getLongetiude());
		
		
		//}
		
		
		
		
		write(response,map);
	}
	
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
