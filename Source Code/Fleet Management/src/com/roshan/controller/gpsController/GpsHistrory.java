package com.roshan.controller.gpsController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblGpsTracking;
import com.roshan.entity.TblVehicle;
import com.roshan.model.GpsModel;

/**
 * Servlet implementation class GpsHistrory
 */
@WebServlet("/pages/gps/GpsHistrory")
public class GpsHistrory extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.ENGLISH);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GpsHistrory() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		//System.out.println(id);
		
		
		
		Date sdtime =null ,satime= null;
		
		try {
			satime = formatter.parse(request.getParameter("sdate"));
			System.out.println("\n\n\n"+request.getParameter("sdate"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			sdtime = formatter.parse(request.getParameter("edate"));
		} catch (Exception e) {
			//System.out.println(e);
		}

		try{
		GpsModel gm = new GpsModel();
		
		TblVehicle tv = new TblVehicle();
		tv.setId(id);
		List<?>  list = gm.gethistoty(tv, satime,sdtime );
		
		
		
			System.out.println(list.size());
		
			String array="" ;
			String point="" ;
			
			String la[] = null;
			String lo[] = null;
			
		for(int j=0; j<list.size(); j++) {		
			//Object[] row = (Object[]) list.get(j);	
			TblGpsTracking tt = (TblGpsTracking)list.get(j);
			
			if(j==0){
				//point = "new google.maps.LatLng("+ tt.getLatitude()+","+tt.getLongetiude()+")";
				map.put("lapoint",  tt.getLatitude());
				map.put("lopoint", tt.getLongetiude());
				
			}
			
			//la[j] = tt.getLatitude();
			//lo[j] = tt.getLongetiude();
			array = array+ ",{lat: parseFloat("+tt.getLatitude()+"), lng: parseFloat("+tt.getLongetiude()+")}";
			
			//System.out.println(tt.getLatitude());
		}
		//map.put("la", la);
		//map.put("lo", lo);
		String narray ="";
				
		if( list.size()!=0){
			narray =	array.substring(1);
		}
		//map.put("point", point);
		map.put("array", narray);
	
		
		
		write(response,map);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		
	}
	
	
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}


}
