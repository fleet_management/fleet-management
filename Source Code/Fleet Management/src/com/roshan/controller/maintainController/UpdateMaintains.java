package com.roshan.controller.maintainController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblMaintainceId;
import com.roshan.entity.TblVehicle;
import com.roshan.ids.Ids;
import com.roshan.model.MaintainModel;

/**
 * Servlet implementation class UpdateMaintains
 */
@WebServlet("/pages/maintain/UpdateMaintains")
public class UpdateMaintains extends HttpServlet {
	private static final long serialVersionUID = 1L;
	MaintainModel mm = new MaintainModel();
	TblMaintaince tm = new TblMaintaince();
	TblMaintainceId tmi = new TblMaintainceId();
	TblVehicle tv = new TblVehicle();
	
	
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
 
    public UpdateMaintains() {
        super();
      
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		

		int num = 0 ;
		int itemid=0, numof_items=0 ,discount=0;
		double sprice = 0  ,totalprice=0;
		Date datetime = null,edate=null;
		String dis =null;
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		
		try {
			num = Integer.parseInt(request.getParameter("vnum"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		
		try {
			datetime = formatter.parse(request.getParameter("idate"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		
		try {
			itemid = Integer.parseInt(request.getParameter("ftype"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		
		try {
			numof_items = Integer.parseInt(request.getParameter("nunit"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		
		try {
			sprice = Double.parseDouble(request.getParameter("stotal"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		
	
		try {
			discount =Integer.parseInt(request.getParameter("discount"));
		} catch (Exception e) {
			//System.out.println(e);
		}
	
	
		try {
			edate = formatter.parse(request.getParameter("edate"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		
		try {
			totalprice = Double.parseDouble(request.getParameter("tvalue"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		String cat = request.getParameter("cat");
		
		
		
		try {
			dis = request.getParameter("acc_disc");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//boolean result = fm.fuelValidate(name);
		boolean result = false;
		Map<String,Object> map = new HashMap<String,Object>();
		
		try{
		map.put("isValid", result);
					
		if(!result){
			
			tv.setId(num);
			
			//int x = Ids.getId();
			tmi.setId(id);
			tmi.setTblVehicleId(num);
			
			
			
			tm.setTblVehicle(tv);
			tm.setId(tmi);
			tm.setStatus((byte)1);
			tm.setPrice(sprice);
			tm.setDiscount(discount);
			tm.setItemsId(itemid);
			tm.setNumOfItems(numof_items);
			tm.setTotalPrice(totalprice);
			tm.setInsertDateTime(datetime);
			tm.setUpdateDateTime(edate);
			tm.setComment(cat);
			tm.setDiscription(dis);
			
			mm.UpdateMaintain(tm);
			
			
		}
		
		}catch(Exception e){
			//System.out.println(e);
		}
		write(response,map);
		
		
		
		
		
	}
	
	
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
