package com.roshan.controller.maintainController;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblFuleType;
import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblMaintainceId;
import com.roshan.entity.TblVehicle;
import com.roshan.model.FuelModel;
import com.roshan.model.MaintainModel;

/**
 * Servlet implementation class GetMaintains
 */
@WebServlet("/pages/maintain/GetMaintains")
public class GetMaintains extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetMaintains() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		MaintainModel mm = new MaintainModel();
		TblMaintaince tm = new TblMaintaince();
		TblMaintainceId tmi = new TblMaintainceId();
		//TblVehicle tv = new TblVehicle();
		
		int id = Integer.parseInt(request.getParameter("id"));
		int vid = Integer.parseInt(request.getParameter("vid"));

			tmi.setId(id);
			tmi.setTblVehicleId(vid);
			tm.setId(tmi);

		TblMaintaince tf = mm.viewMaintain(tm.getId());
		
		map.put("itemid", tf.getItemsId());
		map.put("price", tf.getPrice());
		map.put("discount", tf.getDiscount());
		map.put("tprice", tf.getTotalPrice());
		map.put("nofitems", tf.getNumOfItems());
		map.put("cat", tf.getComment());
		map.put("acc", tf.getDiscription());
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
		
		try {
			map.put("datetime",tf.getInsertDateTime().toString().substring(0, 10) );
			//System.out.println(tf.getInsertDateTime().toString().substring(0, 10));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if(tf.getUpdateDateTime()!=null){
			map.put("etime",tf.getUpdateDateTime().toString().substring(0, 10) );
			}
			//System.out.println(tf.getInsertDateTime().toString().substring(0, 10));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		write(response,map);
		
	}
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
