package com.roshan.controller.maintainController;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblMaintainceId;
import com.roshan.entity.TblVehicle;
import com.roshan.model.MaintainModel;

/**
 * Servlet implementation class DeleteMaintaince
 */
@WebServlet("/pages/maintain/DeleteMaintaince")
public class DeleteMaintaince extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public DeleteMaintaince() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		MaintainModel mm = new MaintainModel();
		TblMaintaince tm = new TblMaintaince();
		TblMaintainceId tmi = new TblMaintainceId();
		TblVehicle tv = new TblVehicle();
		
		int id = Integer.parseInt(request.getParameter("id"));
		int vid = Integer.parseInt(request.getParameter("vid"));

			tmi.setId(id);
			tmi.setTblVehicleId(vid);
			tm.setId(tmi);

		mm.deleteMaintain(tm.getId());
		
	}

}
