package com.roshan.controller.maintainController;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblAccessorie;
import com.roshan.entity.TblFuleType;
import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblVehicle;
import com.roshan.model.AccessorieModel;
import com.roshan.model.FuelModel;
import com.roshan.model.MaintainModel;
import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class SearchMaintain
 */
@WebServlet("/pages/maintain/SearchMaintain")
public class SearchMaintain extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	TblVehicle tv = new TblVehicle();
	TblMaintaince tm = new TblMaintaince();
	VehicleModel vm = new VehicleModel();
	
	
	
	FuelModel fm = new FuelModel();
	AccessorieModel am = new AccessorieModel();
	
	TblAccessorie ta;
	TblFuleType ft;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchMaintain() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	

		MaintainModel mm = new MaintainModel();
		List<?> list = new ArrayList<TblMaintaince>();
		
		
		String word = request.getParameter("word");
		String cat = request.getParameter("cat");
		//System.out.println(word);
		if(word.compareTo("NULL")==0){
			
			list= mm.getAllMaintain(cat);
		}else{
			list = mm.searchMaintain(word,cat);
		}
		
		write(list, response,cat);	
		
		
	}
	
	
	
	
	public void write(List<?> list,HttpServletResponse response,String cat) throws IOException{
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String s="";
		//System.out.println(list.isEmpty());
		int i=1;
		String tabl = null;
	
		for(int j=0; j<list.size(); j++) {
			//System.out.println( tv.getRegistrationNumber());
			
			Object[] row = (Object[]) list.get(j);
			
			
			tv = (TblVehicle)row[1];
			tm = (TblMaintaince)row[0];
			
			try{
				if(cat.equalsIgnoreCase("Insuarance")||cat.equalsIgnoreCase("licence")||cat.equalsIgnoreCase("accedent")){
					tabl = tm.getInsertDateTime().toString();
				}else if(cat.equalsIgnoreCase("accessory")){
					
					ta = am.viewEmployee(tm.getItemsId());
					tabl = ta.getName();
				}else{
					
					ft = fm.viewEmployee(tm.getItemsId());
					tabl = ft.getName();
				}
				
				}catch(Exception e){
					
				}
			
			s=s+"<tr><td >"+i+"</td><td>"+tv.getRegistrationNumber()+"</td><td>"+ tabl+"</td><td>"+ tm.getTotalPrice()+"</td><td>  <a href='#' onclick='MaintainView("+tm.getId().getId()+","+tm.getId().getTblVehicleId()+")' class='btn btn-default btn-xs' data-toggle='modal' data-target='.add_new'><span class='glyphicon glyphicon-exclamation-sign cus' aria-hidden='true'></span>Update</a>  <a onclick='deleteMaintain("+tm.getId().getId()+","+tm.getId().getTblVehicleId()+")' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove cus' aria-hidden='true'></span>Delete</a></td></tr>";
			
			i++;
		}
		out.println(s);
		//System.out.println(s);
	
	}

}
