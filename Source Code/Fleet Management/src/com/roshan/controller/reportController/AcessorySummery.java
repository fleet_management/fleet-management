package com.roshan.controller.reportController;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblFuleType;
import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblSchedule;
import com.roshan.entity.TblVehicle;
import com.roshan.model.FuelModel;
import com.roshan.model.ReportModel;
import com.roshan.model.SheduleModel;
import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class AcessorySummery
 */
@WebServlet("/pages/report/AcessorySummery")
public class AcessorySummery extends HttpServlet {
	private static final long serialVersionUID = 1L;
	TblVehicle tv = new TblVehicle();
	TblMaintaince tm = new TblMaintaince();
	VehicleModel vm = new VehicleModel();
	
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	

    public AcessorySummery() {
        super();
       
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		Date ftime =null ,ttime= null;
		
		try {
			ftime = formatter.parse(request.getParameter("sdate")+" 00:00");
			System.out.println("\n\n\n"+request.getParameter("sdate"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			ttime = formatter.parse(request.getParameter("edate")+" 00:00");
		} catch (Exception e) {
			System.out.println(e);
		}

		
		
		
		ReportModel rm = new ReportModel();
		//SheduleModel sm = new SheduleModel();
		List<?> list = new ArrayList();

			
			list= rm.searchSummer(ftime ,ttime);

		write(list, response);
		
		
		
	}
	public void write(List<?> list,HttpServletResponse response) throws IOException{
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String s="";

		int i=1;
		
			
		double k =0;
		//FuelModel fm = new FuelModel();

		for(int j=0; j<list.size(); j++){
			
			Object[] row = (Object[]) list.get(j);	

			s=s+"<tr><td >"+i+"</td><td>"+row[0]+"</td><td>"+ row[1]+"</td><td>"+ row[2]+"</td></tr>";
			k = k + (Double)row[2];
			i++;
		}
		
		s=s+"<tr><td > </td><td> </td><td> <h5><b>Total Rs :</b></h5> </td><td><h5><b>"+ k+"</b></h5></td></tr>";
		out.println(s);
		//System.out.println(s);
	
		}
	
}