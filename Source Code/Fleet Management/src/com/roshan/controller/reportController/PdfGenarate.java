package com.roshan.controller.reportController;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblVehicle;
import com.roshan.model.ReportModel;
import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class PdfGenarate
 */
@WebServlet("/pages/report/PdfGenarate")
public class PdfGenarate extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	TblVehicle tv = new TblVehicle();
	TblMaintaince tm = new TblMaintaince();
	VehicleModel vm = new VehicleModel();
	
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	Date ftime =null ,ttime= null;  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PdfGenarate() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		
		
		
		try {
			ftime = formatter.parse(request.getParameter("sdate")+" 00:00");
			//System.out.println("\n\n\n"+request.getParameter("sdate"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			ttime = formatter.parse(request.getParameter("edate")+" 00:00");
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		
		
		ReportModel rm = new ReportModel();
		//SheduleModel sm = new SheduleModel();
		List<?> list = new ArrayList();

			
		list= rm.searchSummer(ftime ,ttime);
		
		System.out.println(list.size());
		
		response.setContentType("application/pdf");
		
		try{
		  Document doc = new Document();
          
          PdfWriter.getInstance(doc, response.getOutputStream());
          
   
          doc.addCreationDate();
          doc.addProducer();

          doc.addTitle("Summery Report");
          doc.setPageSize(PageSize.LETTER);
          doc.open();
          
          
          
          
         //doc.add(new Paragraph("Helloe") );
          
          createDoc(doc, list,request.getParameter("sdate"),request.getParameter("edate"));
          
          
          doc.close();
          
          
		}catch(Exception e){
			
		}
		
		
	}
	
	
	
	public Document createDoc(Document doc ,List list,String sdate ,String tdate) throws DocumentException {
		
		
		
		
		
		
		
	   doc.add(new Paragraph("***************************************************************************************************************"));
       //doc.add(new Paragraph("SATYODAYA WOMEN'S PROGRAMME : MICRO CREDIT SCHEME ",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
       doc.add(new Paragraph("Summary Report  ",FontFactory.getFont(FontFactory.TIMES_ROMAN,12, BaseColor.RED)) );                                                                                                                     
       doc.add(new Paragraph("From   "+sdate+"  To  "+ tdate,FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
       doc.add(new Paragraph("Summary Report  ",FontFactory.getFont(FontFactory.TIMES_ROMAN,12, BaseColor.WHITE)) );              
        
       PdfPTable LogTable = new PdfPTable(4);  
       //create a cell object  
          
       PdfPCell table_cell; 
       LogTable.setWidthPercentage(100f);
       LogTable.getDefaultCell().setUseAscender(true);
       LogTable.getDefaultCell().setUseDescender(true);
      LogTable.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
     
       
       
	   LogTable.addCell("No");
	   
	   LogTable.addCell("Maintaince");
	   LogTable.addCell("Number Of Maintance");
	   LogTable.addCell("Total Rs :- ");
	   
	   
	   float[] columnWidths = new float[] {10f,30f,30f,30f};
       LogTable.setWidths(columnWidths);
	
	      


       	double k=0;
		for(int j=0; j<list.size(); j++){
			
			Object[] row = (Object[]) list.get(j);	
			//System.out.println(j);
			 table_cell=new PdfPCell(new Phrase(Integer.toString(j+1),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
			 table_cell.setBorder(Rectangle.NO_BORDER);
             LogTable.addCell(table_cell); 
             
             table_cell=new PdfPCell(new Phrase((String) row[0],FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
             table_cell.setBorder(Rectangle.NO_BORDER);
             LogTable.addCell(table_cell);
            
             try{
            	 Long aa =  (Long) row[1];
             
             table_cell=new PdfPCell(new Phrase(Long.toString(aa),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
             table_cell.setBorder(Rectangle.NO_BORDER);
             LogTable.addCell(table_cell); 
             }catch(Exception e){
            	 
            	 e.printStackTrace();
             }
            double d = (Double)row[2];
             table_cell=new PdfPCell(new Phrase(Double.toString(d),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)));  
             table_cell.setBorder(Rectangle.NO_BORDER);
             LogTable.addCell(table_cell); 

			//s=s+"<tr><td >"+i+"</td><td>"+row[0]+"</td><td>"+ row[1]+"</td><td>"+ row[2]+"</td></tr>";
			k = k + (Double)row[2];
			
		}
		
		
	
          
		   table_cell=new PdfPCell(new Phrase(""));  
           table_cell.setBorder(Rectangle.NO_BORDER);
           LogTable.addCell(table_cell); 
           
          
           table_cell=new PdfPCell(new Phrase(""));  
           table_cell.setBorder(Rectangle.NO_BORDER);
           LogTable.addCell(table_cell);
           
           table_cell=new PdfPCell(new Phrase("Total Rs :- ",FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.RED)));  
           table_cell.setBorder(Rectangle.NO_BORDER);
           LogTable.addCell(table_cell); 
           
          
           table_cell=new PdfPCell(new Phrase(Double.toString(k),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.RED)));  
           table_cell.setBorder(Rectangle.NO_BORDER);
           LogTable.addCell(table_cell); 

		// doc.add(new Paragraph("    Total Rs :- "+ Double.toString(k),FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, BaseColor.BLACK)) );
		//s=s+"<tr><td > </td><td> </td><td> <h5><b>Total Rs :</b></h5> </td><td><h5><b>"+ k+"</b></h5></td></tr>";
	
		
		
		
		doc.add(LogTable);
		
		
		return doc;
		
	}

}
