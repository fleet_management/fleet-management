package com.roshan.controller.reportController;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblSchedule;
import com.roshan.entity.TblVehicle;
import com.roshan.model.ReportModel;
import com.roshan.model.SheduleModel;
import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class AccessorySearch
 */
@WebServlet("/pages/report/AccessorySearch")
public class AccessorySearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
	TblVehicle tv = new TblVehicle();
	//TblSchedule ts = new TblSchedule();
	TblMaintaince tm = new TblMaintaince();
	VehicleModel vm = new VehicleModel();
	
	
	
	
	
    public AccessorySearch() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ReportModel rm = new ReportModel();
			//SheduleModel sm = new SheduleModel();
			List<?> list = new ArrayList<TblMaintaince>();

				
				list= rm.searchAccReport("fuel");

			write(list, response);
		
		
	}	
		
		public void write(List<?> list,HttpServletResponse response) throws IOException{
			
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			String s="";

			int i=1;

			for(int j=0; j<list.size(); j++) {		
				Object[] row = (Object[]) list.get(j);	
				tm = (TblMaintaince)row[0];
				tv = (TblVehicle)row[1];
				
				s=s+"<tr><td >"+i+"</td><td>"+tv.getRegistrationNumber()+"</td><td>"+ tm.getInsertDateTime().toString().substring(0,10)+"</td><td>"+ tm.getNumOfItems()+"</td><td>"+ tm.getTotalPrice()+"</td></tr>";
				
				i++;
			}
			out.println(s);
			//System.out.println(s);
		
			}
		
}