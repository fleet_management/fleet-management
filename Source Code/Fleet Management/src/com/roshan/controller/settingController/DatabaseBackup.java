package com.roshan.controller.settingController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class DatabaseBackup
 */
@WebServlet("/pages/setting/DatabaseBackup")
public class DatabaseBackup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DatabaseBackup() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		Map<String,Object> map = new HashMap<String,Object>();
		Process p = null;
        try {
            Runtime runtime = Runtime.getRuntime();
            
            
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            
            p = runtime.exec("C:/xampp/mysql/bin/mysqldump -ufleet -pfleet --add-drop-database -B fleet -r " + "D:\\Backup\\" + date + ".sql");
//change the dbpass and dbname with your dbpass and dbname
            int processComplete = p.waitFor();

            if (processComplete == 0) {

                System.out.println("Backup created successfully!");
                map.put("date", date);
                write(response,map);

            } else {
            	 System.out.println("Could not create the backup");
                //lblMessage.setText("Could not create the backup");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
		
		
	}
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
