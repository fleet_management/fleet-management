package com.roshan.controller.settingController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.MstOrganization;
import com.roshan.entity.TblEmployee;
import com.roshan.model.SettingModel;

/**
 * Servlet implementation class PasswordChange
 */
@WebServlet("/pages/setting/PasswordChange")
public class PasswordChange extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PasswordChange() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String cpwd = request.getParameter("cpwd");
		String npwd = request.getParameter("pwd1");
		 int id = Integer.parseInt(request.getParameter("id"));
		 Map<String,Object> map = new HashMap<String,Object>();
		 
		 SettingModel sm = new SettingModel();
		 boolean result = sm.pwdValidate(cpwd);
		 map.put("isValid", result);
		 try{
			 
			 if(result){
				 
				 TblEmployee te = new TblEmployee();
				 MstOrganization org = new MstOrganization();
				 org.setId("1");
				 te.setId(id);
				 te.setPassword(npwd);
				 te.setMstOrganization(org);
				 
				 sm.updatePassword(te);
			 }
			 
			 
			 
		 }catch(Exception e){
			 System.out.println(e);
		 }
		
		 write(response,map);
		
	}
	
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
