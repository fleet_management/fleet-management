package com.roshan.controller.settingController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.MstOrganization;
import com.roshan.entity.TblFuleType;
import com.roshan.model.FuelModel;
import com.roshan.model.SettingModel;

/**
 * Servlet implementation class GetMainDetails
 */
@WebServlet("/pages/setting/GetMainDetails")
public class GetMainDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetMainDetails() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		Map<String,Object> map = new HashMap<String,Object>();
		String id = "1";
		
		try {
		id  = request.getParameter("id");
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		

		try{
		SettingModel sm = new SettingModel();
		MstOrganization mo  = sm.getOrganization(id);
		map.put("name", mo.getName());
		map.put("tel", mo.getTelephone());
		map.put("web", mo.getWeb());
		map.put("address", mo.getAddresss());
		map.put("email", mo.getEmail());
		map.put("fax", mo.getFax());
		
		write(response,map);
		
		}catch (Exception e) {
			//System.out.println(e);
		}
		
		
	}
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
