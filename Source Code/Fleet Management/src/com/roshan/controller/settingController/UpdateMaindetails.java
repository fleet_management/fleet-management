package com.roshan.controller.settingController;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.MstOrganization;
import com.roshan.model.SettingModel;

/**
 * Servlet implementation class UpdateMaindetails
 */
@WebServlet("/pages/setting/UpdateMaindetails")
public class UpdateMaindetails extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UpdateMaindetails() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		MstOrganization mo = new MstOrganization();
		
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String fax = request.getParameter("fax");
		String email = request.getParameter("email");
		String tel = request.getParameter("tel");
		String web = request.getParameter("web");
		
		
		mo.setAddresss(address);
		mo.setEmail(email);
		mo.setWeb(web);
		mo.setFax(fax);
		mo.setTelephone(tel);
		mo.setName(name);
		
		mo.setId("1");
		
		SettingModel sm = new SettingModel();
		sm.editOrganization(mo);
		
		
	}

}
