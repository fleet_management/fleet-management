package com.roshan.controller.accessoryController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblAccessorie;
import com.roshan.entity.TblFuleType;
import com.roshan.model.AccessorieModel;
import com.roshan.model.FuelModel;

/**
 * Servlet implementation class GetFuel
 */
@WebServlet("/pages/maintain/GetAccessory")
public class GetAccessory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAccessory() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Map<String,Object> map = new HashMap<String,Object>();
		int id = Integer.parseInt(request.getParameter("id"));
		
try {
	
	AccessorieModel fm = new AccessorieModel();
	TblAccessorie ac = fm.viewEmployee(id);
	map.put("name", ac.getName());
	map.put("price", ac.getPrice());
	//map.put("cat", ac.getTblCatogory().getId());
	map.put("brand", ac.getTblBrand().getId());
	//map.put("vender");
	map.put("id", ac.getComment());
	map.put("utype", ac.getUnits());
	

	write(response,map);
	
} catch (Exception e) {
	System.out.println(e);
}
		

		
	}
	
	
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
