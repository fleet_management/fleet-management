package com.roshan.controller.accessoryController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.model.AccessorieModel;
import com.roshan.model.FuelModel;
import java.util.List;

import com.roshan.entity.TblAccessorie;
import com.roshan.entity.TblFuleType;



@WebServlet("/pages/maintain/ReadsDataAccessory")
public class ReadsAccRecord extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ReadsAccRecord() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String word = request.getParameter("word");
		AccessorieModel fm = new AccessorieModel();
		List<TblAccessorie> list = fm.getAllAccessory();
		
		write(list, response);
		
	}

	public void write(List<TblAccessorie> list,HttpServletResponse response) throws IOException{
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String s="";
		//System.out.println(list.isEmpty());
		int i=1;
		for(TblAccessorie te : list){
			//System.out.println( te.getId());
			s=s+"<tr><td >"+i+"</td><td>"+te.getName()+"</td><td>"+ te.getPrice()+"</td><td>  <a href='#' onclick='accView("+te.getId()+")' class='btn btn-default btn-xs' data-toggle='modal' data-target='.add_new'><span class='glyphicon glyphicon-exclamation-sign cus' aria-hidden='true'></span>Update</a> <a onclick='deleteacc("+te.getId()+")' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove cus' aria-hidden='true'></span>Delete</a></td></tr>";
			
			i++;
		}
		out.println(s);
		//System.out.println(s);
	
	}
	
	
}
