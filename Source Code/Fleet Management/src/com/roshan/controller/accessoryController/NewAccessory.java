package com.roshan.controller.accessoryController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblAccessorie;
import com.roshan.entity.TblAccessoriesPriceHistoty;
import com.roshan.entity.TblAccessoriesPriceHistotyId;
import com.roshan.entity.TblBrand;
import com.roshan.entity.TblCatogory;
import com.roshan.entity.TblFulePriceHistoty;
import com.roshan.entity.TblFulePriceHistotyId;
import com.roshan.entity.TblFuleType;
import com.roshan.entity.TblVender;
import com.roshan.ids.Ids;
import com.roshan.model.AccessorieModel;
import com.roshan.model.FuelModel;

/**
 * Servlet implementation class NewFuel
 */
@WebServlet("/pages/maintain/NewAccessory")
public class NewAccessory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	 AccessorieModel am = new AccessorieModel();
	TblAccessorie a = new TblAccessorie();
	TblAccessoriesPriceHistoty ah = new TblAccessoriesPriceHistoty();
	TblAccessoriesPriceHistotyId ahi = new TblAccessoriesPriceHistotyId();
	
	TblBrand tb = new TblBrand();
	//TblCatogory tc = new TblCatogory();
	TblVender tv = new TblVender();
	Set<TblVender> tblVenders = new HashSet<TblVender>(0);
	
	Date date = new Date();
	
    public NewAccessory() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int cat,brand,vender;
		String name = request.getParameter("name");
		String id = request.getParameter("id");
		String unit = request.getParameter("utype");
		try {
			 //cat = Integer.parseInt(request.getParameter("cat"));
			// tc.setId(1);
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		
		
		try {
			 brand  = Integer.parseInt(request.getParameter("brand"));
			 tb.setId(1);
			 
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		try {
			 vender  = Integer.parseInt(request.getParameter("vender"));
			 tv.setId(1);
			 tblVenders.add(tv);
		} catch (Exception e) {
			System.out.println(e);
		}



		boolean result = am.AccessoryValidate(name);
		Map<String,Object> map = new HashMap<String,Object>();
		
		try{
		double price = Double.parseDouble(request.getParameter("price"));
		map.put("isValid", result);
					
		if(!result){
			
			int x = Ids.getId();
			a.setId(x);
			a.setName(name);
			a.setPrice(price);
			a.setStatus((byte)1);
			a.setUnits(unit);
			a.setTblBrand(tb);
			//a.setTblCatogory(tc);
			a.setComment(id);
			a.setTblVenders(tblVenders);
			//
			//a.sett
			
			
			//ahi.setTblAccessoriesId(x);;
			//int y = Ids.getId()+1;
			//ahi.setId(y);
			
			
			
			//ah.setPrice(price);
			//ah.setDate(date);
			//ah.setTblAccessorie(a);
			//ah.setId(ahi);
			//ah.setStatus("1");
		
		
			
			
			am.addAccessory(a);
			
			
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		write(response,map);
		
		
	}
	
private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
