package com.roshan.controller.mobileController;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblGpsTracking;
import com.roshan.entity.TblGpsTrackingId;
import com.roshan.entity.TblVehicle;
import com.roshan.ids.Ids;
import com.roshan.model.MobileAccess;

/**
 * Servlet implementation class Gps
 */
@WebServlet("/Gps")
public class Gps extends HttpServlet {
	private static final long serialVersionUID = 1L;
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm",Locale.ENGLISH);

    public Gps() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		try{
			
		
		String lati = request.getParameter("lati");
		int id = Integer.parseInt(request.getParameter("vehiclenum"));
		String lonti = request.getParameter("longt");
		System.out.println(lati);
		System.out.println(id);
		System.out.println(lonti);
		
		TblGpsTracking tg = new TblGpsTracking();
		TblGpsTrackingId tgt = new TblGpsTrackingId();
		TblVehicle tv = new TblVehicle();
		tv.setId(id);
				
		tgt.setTblVehicleId(id);
		int x = Ids.getId();
		tgt.setId(x);
		
		tg.setId(tgt);
		tg.setLatitude(lati);
		tg.setLongetiude(lonti);
		tg.setTblVehicle(tv);
		
		Date date = new Date();
		
		tg.setTrackingTime(formatter.parse(formatter.format(date)));
		
		
		MobileAccess ma = new MobileAccess();
		ma.getGpstrack(tg);
		
		
		
		}catch(Exception e){
			
			e.printStackTrace();
		}
		
		
		
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String s="hellow";
		out.println(s);
		
	}

	
}
