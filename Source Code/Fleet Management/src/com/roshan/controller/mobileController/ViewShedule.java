package com.roshan.controller.mobileController;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblEmployee;
import com.roshan.entity.TblSchedule;
import com.roshan.entity.TblVehicle;
import com.roshan.model.SheduleModel;
import com.roshan.model.VehicleModel;


@WebServlet("/ViewShedule")
public class ViewShedule extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	TblVehicle tv = new TblVehicle();
	TblSchedule ts = new TblSchedule();
	VehicleModel vm = new VehicleModel();
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.ENGLISH);
	
	TblEmployee te = new TblEmployee();
    public ViewShedule() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		Date sdtime =null ,satime= null;
		//System.out.println(request.getParameter("sdtime"));
		//System.out.println(request.getParameter("satime"));
		try {
			satime = formatter.parse(request.getParameter("sdtime")+" 00:00");
			System.out.println("\n\n\n"+request.getParameter("sdtime")+" 00:01");
		} catch (Exception e) {
			System.out.println(request.getParameter("sdtime"));
		}
		
		try {
			sdtime = formatter.parse(request.getParameter("satime")+" 00:00");
		} catch (Exception e) {
			//System.out.println(e);
		}

		
		
		SheduleModel sm = new SheduleModel();
		
		
		
		List<?> list = sm.SelectSedule(sdtime, satime);
		
		write(list, response);
	}
	
	
	public void write(List<?> list,HttpServletResponse response) throws IOException{
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String s="<table data-role='table' id='movie-table-custom' data-mode='reflow' class='movie-list ui-responsive'><thead><tr><th>Arrival Date</th><th data-priority='2'>Departure Date</th><th data-priority='3'>From</th><th data-priority='4'>To</th><th data-priority='4'>Vehicle</th></tr></thead><tbody id ='table_body'>";

		int i=1;

		for(int j=0; j<list.size(); j++) {		
			Object[] row = (Object[]) list.get(j);	
			ts = (TblSchedule)row[0];
			tv = (TblVehicle)row[1];
			
			s=s+"<tr><td >"+i+"</td><td>"+tv.getRegistrationNumber()+"</td><td>"+ ts.getInsertDateTime()+"</td><td>"+ ts.getStartFrom()+"</td><td>"+ ts.getEndTo()+"</td></tr>";
			;
			i++;
		}
		s =  s+"</tbody></table>";
		out.println(s);
		System.out.println(s);
	
		}
}
