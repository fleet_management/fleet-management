package com.roshan.controller.vehicleController;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class DeleteVehicle
 */
@WebServlet("/pages/vehicles/DeleteVehicle")
public class DeleteVehicle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteVehicle() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		VehicleModel vm = new VehicleModel();
		int id = Integer.parseInt(request.getParameter("id"));
		vm.deleteVehicle(id);
		
	}

}
