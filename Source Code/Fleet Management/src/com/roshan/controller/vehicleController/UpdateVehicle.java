package com.roshan.controller.vehicleController;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.MstOrganization;
import com.roshan.entity.TblVehicle;
import com.roshan.ids.Ids;
import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class UpdateVehicle
 */
@WebServlet("/pages/vehicles/UpdateVehicle")
public class UpdateVehicle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	MstOrganization org = new MstOrganization();
	VehicleModel vehiclemodel = new VehicleModel();
	TblVehicle tv = new TblVehicle();
	boolean result;
	
	
    public UpdateVehicle() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
org.setId("1");
		
		String vehicle_num=null,model=null,class_type=null,catogory =null ,capacity=null, fuel=null,engine=null,chasi=null;
		String licece =null, i_company=null;
		
		try {
			 vehicle_num = request.getParameter("vehicle_num");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		try {
			 model = request.getParameter("model");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			class_type = request.getParameter("class_type");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 catogory = request.getParameter("catogory");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 capacity = request.getParameter("capacity");
		} catch (Exception e) {
			System.out.println(e);
		}
	
		try {
			 fuel = request.getParameter("fuel");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 engine = request.getParameter("engine");
		} catch (Exception e) {
			System.out.println(e);
		}
	
		try {
			 chasi = request.getParameter("chasi");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 licece = request.getParameter("licece");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 i_company = request.getParameter("i_company");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		Date expiredate=null;

         try {
        	expiredate = formatter.parse(request.getParameter("expire_date"));
			
		} catch (ParseException e) {
			//e.printStackTrace();
		}
         
         
         String dep_per=null,price=null,wheels=null;
         String heigth=null,width=null,length=null,year=null;
         
         
     	try {
     		dep_per = request.getParameter("dep_per");
		} catch (Exception e) {
			System.out.println(e);
		}
		
     	
    	try {
    		price = request.getParameter("price");
		} catch (Exception e) {
			System.out.println(e);
		}
		
    	
    	try {
    		wheels = request.getParameter("wheels");
		} catch (Exception e) {
			System.out.println(e);
		}
		
    	
    	try {
    		heigth =request.getParameter("height");
		} catch (Exception e) {
			System.out.println(e);
		}
    	
    	
    	try {
    		width = request.getParameter("width");
		} catch (Exception e) {
			System.out.println(e);
		}
    	
    	
    	try {
    		length =request.getParameter("length");
		} catch (Exception e) {
			System.out.println(e);
		}
    	
    	
    	Date date_import=null,date_regis=null;

        try {
        	date_import = formatter.parse(request.getParameter("date_import"));
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
        
        try {
        	date_regis = formatter.parse(request.getParameter("date_regis"));
   			
   		} catch (ParseException e) {
   			e.printStackTrace();
   		}
        
        
        
        
        try {
          	year = request.getParameter("year_manufac");
   			
   		} catch (Exception e) {
   			e.printStackTrace();
   		}
		

        	int id=0;
    	try {
    		id = Integer.parseInt(request.getParameter("id"));
		} catch (Exception e) {
			System.out.println(e);
		}
    	
		
		
	
		//String photo = request.getParameter("photo");
		//String document = request.getParameter("document");
		
		
		result = vehiclemodel.VehicleUpdateValidate(vehicle_num);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("isValid", result);
		
		if(!result){
			
			tv.setMstOrganization(org);
			tv.setId(id);
			
			
			tv.setRegistrationNumber(vehicle_num);
			tv.setModel(model);
			tv.setClassType(class_type);
			tv.setCapacity(capacity);
			tv.setFuelType(fuel);
			tv.setEngineNumber(engine);
			tv.setInsuaranceName(i_company);
			tv.setDepreciationPerYear(dep_per);
			tv.setPurchasedPrice(price);
			tv.setNumberOfWheels(wheels);
			tv.setHeight(heigth);
			tv.setWidth(width);
			tv.setLength(length);
			tv.setDateImport(date_import);
			tv.setDateRegister(date_regis);
			tv.setYearManufacture(year);
			tv.setDepreciationPerYear(dep_per);
			
			tv.setCatogory(catogory);
			tv.setLicence(licece);
			tv.setChasi(chasi);
			tv.setExpiteDate(expiredate);
			
			tv.setStatus("1");

			
			
			vehiclemodel.UpdateVehicle(tv);;
			
		}
		 write(response,map);
		
		
		
		
	}
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}
	
	

}
