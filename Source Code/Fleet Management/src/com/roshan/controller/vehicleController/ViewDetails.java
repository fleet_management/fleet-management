package com.roshan.controller.vehicleController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblVehicle;
import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class ViewDetails
 */
@WebServlet("/pages/vehicles/ViewDetails")
public class ViewDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewDetails() {
        super();

    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		Map<String,Object> map = new HashMap<String,Object>();
		int id = Integer.parseInt(request.getParameter("id"));
		
		VehicleModel vm = new VehicleModel();
		TblVehicle tv = new TblVehicle();
		
		tv = vm.viewEmployee(id);
		
		map.put("vehicle_num", tv.getRegistrationNumber());
		map.put("model", tv.getModel());
		map.put("class_type",tv.getClassType());
		map.put("catogory", tv.getCatogory());
		map.put("capacity", tv.getCapacity());
		map.put("fuel",tv.getFuelType());
		map.put("engine", tv.getEngineNumber());
		map.put("chasi", tv.getChasi());
		map.put("licece", tv.getLicence());
		
		try{
		map.put("expire_date", tv.getExpiteDate().toString().substring(0, 10));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		map.put("i_company", tv.getInsuaranceName());
		map.put("year_manufac", tv.getYearManufacture());
		
		try{
		map.put("date_import", tv.getDateImport().toString().substring(0, 10));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try{
		map.put("date_regis", tv.getDateRegister().toString().substring(0, 10));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		map.put("dep_per", tv.getDepreciationPerYear());
		map.put("price", tv.getPurchasedPrice());
		map.put("wheels", tv.getNumberOfWheels());
		map.put("height", tv.getHeight());
		map.put("width", tv.getWidth());
		map.put("length", tv.getLength());
		
		
		
		
		write(response,map);
		
	}
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
