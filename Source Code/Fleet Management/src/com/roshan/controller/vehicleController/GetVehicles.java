package com.roshan.controller.vehicleController;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblVehicle;
import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class GetVehicles
 */
@WebServlet("/pages/vehicles/SearchVehicle")
public class GetVehicles extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	List<TblVehicle> l = new ArrayList<TblVehicle>();
	VehicleModel em = new VehicleModel();
	
    public GetVehicles() {
        super();
       
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		String word = request.getParameter("word");
		if(word.compareTo("NULL")==0){
			
			l= em.getAllVehicle();
		}else{
			l= em.searchVehicle(word);
		}

			write(l, response);
		
		
		
	}
	
	
	public void write(List<TblVehicle> l,HttpServletResponse response) throws IOException{
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String s="";
		//System.out.println(l.isEmpty());
		int i = 1;
		for(TblVehicle te : l){
			System.out.println( te.getId());
			s=s+"<tr><td >"+ i+"</td><td>"+te.getRegistrationNumber()+"</td><td>"+ te.getCapacity()+"</td><td>"+ te.getExpiteDate()+"</td><td>  <a href='UpdateVehicle.jsp?q="+te.getId()+"' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-exclamation-sign cus' aria-hidden='true'></span> Update </a> <a onclick='deleteVehicle("+te.getId()+")'  class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove cus' aria-hidden='true'></span>Delete</a></td></tr>";
			
			i ++;
		}
		out.println(s);
	//System.out.println(s);
	
	}

}
