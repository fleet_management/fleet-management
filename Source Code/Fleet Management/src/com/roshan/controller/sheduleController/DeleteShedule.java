package com.roshan.controller.sheduleController;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.model.SheduleModel;

/**
 * Servlet implementation class DeleteShedule
 */
@WebServlet("/pages/shedule/DeleteShedule")
public class DeleteShedule extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteShedule() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		SheduleModel sm = new SheduleModel();
		sm.deleteShedule(id);
		
	}

}
