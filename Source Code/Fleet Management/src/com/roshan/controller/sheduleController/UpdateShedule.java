package com.roshan.controller.sheduleController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblEmployee;
import com.roshan.entity.TblSchedule;
import com.roshan.entity.TblVehicle;
import com.roshan.ids.Ids;
import com.roshan.model.SheduleModel;

/**
 * Servlet implementation class UpdateShedule
 */
@WebServlet("/pages/shedule/UpdateShedule")
public class UpdateShedule extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	SheduleModel sm = new SheduleModel();
	TblSchedule ts = new TblSchedule();
	TblEmployee te = new TblEmployee();
	TblVehicle tv = new TblVehicle();
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	
    public UpdateShedule() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		
		
		
		String from=null,to = null,she_disc=null;
		int vnum = 0 , elnum = 0;
		Date dtime =null ,atime= null;
		
		Map<String,Object> map = new HashMap<String,Object>();
		int id = Integer.parseInt(request.getParameter("id"));
	
		try {
			vnum = Integer.parseInt(request.getParameter("vnum"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			elnum = Integer.parseInt(request.getParameter("enum"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		try {
			from = request.getParameter("from");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			to = request.getParameter("to");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		try {
			she_disc = request.getParameter("she_disc");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		try {
			atime = formatter.parse(request.getParameter("dtime"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		try {
			dtime = formatter.parse(request.getParameter("atime"));
		} catch (Exception e) {
			//System.out.println(e);
		}
		
		//dtime
		//atime
		boolean result = false;
	
		map.put("isValid", result);
		
		
		if(!result){
			
			tv.setId(vnum);
			Set<TblVehicle> tvs = new HashSet<TblVehicle>();
			tvs.add(tv);
			
			te.setId(elnum);
			Set<TblEmployee> tve = new HashSet<TblEmployee>();
			tve.add(te);
			
		//	int x = Ids.getId();
			
		
			ts.setId(id);
			//ts.setVehicleNum(vnum);
			//ts.setEmployeeId(elnum);
			
			ts.setTblEmployees(tve);
			ts.setTblVehicles(tvs);
			ts.setEndTo(to);
			ts.setDiscription(she_disc);
			ts.setStartFrom(from);
			ts.setInsertDateTime(dtime);
			ts.setStartDateTime(atime);
			ts.setStatus((byte)1);
			
			sm.UpdateMaintain(ts);
			
		
			write(response,map);
		}
	
		
	}
		
		private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
			

			response.setContentType("json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(new Gson().toJson(map));
		
		}


}
