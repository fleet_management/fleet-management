package com.roshan.controller.sheduleController;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblEmployee;
import com.roshan.entity.TblSchedule;
import com.roshan.entity.TblVehicle;
import com.roshan.model.MaintainModel;
import com.roshan.model.SheduleModel;
import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class GetData
 */
@WebServlet("/pages/shedule/GetData")
public class GetData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	TblVehicle tv = new TblVehicle();
	TblSchedule ts = new TblSchedule();

	SheduleModel sm = new SheduleModel();
	
	TblEmployee te = new TblEmployee();
	
    public GetData() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		Map<String,Object> map = new HashMap<String,Object>();
		int id = Integer.parseInt(request.getParameter("id"));		
		List<?> list = sm.viewMaintain(id);;
		
		for(int j=0; j<list.size(); j++) {		
			Object[] row = (Object[]) list.get(j);	
			ts = (TblSchedule)row[0];
			tv = (TblVehicle)row[1];
			te =(TblEmployee)row[2];
			
			map.put("vnum", tv.getId());
			map.put("enum", te.getId());
			map.put("to", ts.getEndTo());
			map.put("from", ts.getStartFrom());
			map.put("she_disc", ts.getDiscription());
		
		
		}
		
	
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	

		
		try {
			map.put("dtime",ts.getInsertDateTime().toString() );
		} catch (Exception e) {
			 //TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			map.put("atime",ts.getStartDateTime().toString().substring(0, 15));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		write(response,map);
	}
	
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
