package com.roshan.controller.sheduleController;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblSchedule;
import com.roshan.entity.TblVehicle;
import com.roshan.model.SheduleModel;
import com.roshan.model.VehicleModel;

/**
 * Servlet implementation class SearchSedule
 */
@WebServlet("/pages/shedule/SearchSedule")
public class SearchSedule extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	TblVehicle tv = new TblVehicle();
	TblSchedule ts = new TblSchedule();
	VehicleModel vm = new VehicleModel();
	
	
    public SearchSedule() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		 String word = request.getParameter("word");
		SheduleModel sm = new SheduleModel();
		List<?> list = new ArrayList<TblSchedule>();
		
		
		
		if(word.compareTo("NULL")==0){
			
			list= sm.getAllShedule();
		}else{
			list = sm.searchShedule(word);
		}
		
		

		
		write(list, response);
		
		
		
	}
	
	
	
	public void write(List<?> list,HttpServletResponse response) throws IOException{
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String s="";

		int i=1;

		for(int j=0; j<list.size(); j++) {		
			Object[] row = (Object[]) list.get(j);	
			ts = (TblSchedule)row[0];
			tv = (TblVehicle)row[1];
			
			s=s+"<tr><td >"+i+"</td><td>"+tv.getRegistrationNumber()+"</td><td>"+ ts.getInsertDateTime()+"</td><td>"+ ts.getStartFrom()+"</td><td>"+ ts.getEndTo()+"</td><td>  <a href='#' onclick='SheduleView("+ts.getId()+")' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-exclamation-sign cus' aria-hidden='true'></span>Update</a>  <a onclick='deleteShedule("+ts.getId()+")' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove cus' aria-hidden='true'></span>Delete</a></td></tr>";
			
			i++;
		}
		out.println(s);
		//System.out.println(s);
	
		}

}
