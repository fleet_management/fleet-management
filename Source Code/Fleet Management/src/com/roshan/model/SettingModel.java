package com.roshan.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.roshan.entity.MstOrganization;
import com.roshan.entity.TblEmployee;
import com.roshan.entity.TblFuleType;

public class SettingModel {

	
	public MstOrganization getOrganization(String id){
		
		
		MstOrganization mo = null;
		try{
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		mo = (MstOrganization) session.get(MstOrganization.class, id);
		transaction.commit();
		session.close();
		sessionfactory.close();
			
		}catch(Exception e){
			
			System.out.println(e);
		}
		return mo;
		
	}
	
	
	public void editOrganization(MstOrganization mo){
		
		
		
		
			try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();				
			session.update(mo);			
			transaction.commit();
			session.close();
			sessionfactory.close();
			
			
			
		
				
			}catch(Exception e){
				
				System.out.println(e);
			}

		
	}
	
	public boolean pwdValidate(String num) {
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblEmployee where password = :num ");
			query.setParameter("num", num);
			//query.setParameter("stat", (byte)1);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			if(users.isEmpty()){			
				result = false;}	
		
		   return result;
	}
	
	public void updatePassword(TblEmployee te){
		
		
		
		
		try{
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();				
		session.update(te);			
		transaction.commit();
		session.close();
		sessionfactory.close();
		
		
		
	
			
		}catch(Exception e){
			
			System.out.println(e);
		}

	
}
	
	
	
}
