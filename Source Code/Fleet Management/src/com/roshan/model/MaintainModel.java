package com.roshan.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.roshan.Interfaces.Maintain;
import com.roshan.entity.TblFuleType;
import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblMaintainceId;

public class MaintainModel implements Maintain {

	
	@Override
	public void addMaintain(TblMaintaince tm) {
	
			try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(tm);
			transaction.commit();
			session.close();
			sessionfactory.close();	

			}catch(Exception e){
				
				System.out.println(e);
			}

	}

	@Override
	public void UpdateMaintain(TblMaintaince tm) {
	

try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();				
			session.update(tm);			
			transaction.commit();
			session.close();
			sessionfactory.close();
			
	
			}catch(Exception e){
				
				System.out.println(e);
			}

	}
		
		
		
		
	

	
	@Override
	public void deleteMaintain(TblMaintainceId mid) {
		
		
		
		
	try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			TblMaintaince maintains = (TblMaintaince)session.get(TblMaintaince.class, mid);
			maintains.setStatus((byte)3);			
			session.update(maintains);		
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){
				
				System.out.println(e);
			}

	}

	@Override
	public TblMaintaince viewMaintain(TblMaintainceId mid) {
		TblMaintaince maintains = null;
		try{
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		maintains = (TblMaintaince) session.get(TblMaintaince.class, mid);
		transaction.commit();
		session.close();
		sessionfactory.close();
			
		}catch(Exception e){
			
			System.out.println(e);
		}
		return maintains;
	}

	@Override
	public List getAllMaintain(String cat) {
		
		
		List<?> fuel = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			Query query = session.createQuery("from TblMaintaince as tm inner join tm.tblVehicle as tv  where tm.status = :num and tm.comment = :cat ");
			query.setParameter("num", (byte)1);
			query.setParameter("cat", cat);
			fuel = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return fuel;
	}

	@Override
	public List searchMaintain(String num,String cat) {
		//boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblMaintaince as tm inner join tm.tblVehicle as tv where tv.registrationNumber  LIKE :num and tm.status = :stat and tm.comment = :cat");
			query.setParameter("num", "%"+num+"%");
			query.setParameter("stat", (byte)1);
			query.setParameter("cat", cat);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			//if(users.isEmpty()){			
				//result = false;}	
		
		   return users;
	}

	@Override
	public boolean MaintainValidate(String num) {
		// TODO Auto-generated method stub
		return false;
	}


}
