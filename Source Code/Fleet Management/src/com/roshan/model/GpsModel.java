package com.roshan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.roshan.entity.TblGpsTracking;
import com.roshan.entity.TblVehicle;

public class GpsModel {
	
	
	public List getdata(TblVehicle Vehicleid){
		
		List cur = new ArrayList();
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			Query query = session.createQuery("from TblGpsTracking where tblVehicle = :num order by trackingTime desc  ").setMaxResults(1);
			query.setParameter("num", Vehicleid);
			cur = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
			
			
		}catch(Exception e){
			
			e.printStackTrace();
		}
		
		
		return cur;
	}
	
	
public List gethistoty(TblVehicle Vehicleid,Date stime , Date etime){
		
		List cur = new ArrayList();
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			Query query = session.createQuery("from TblGpsTracking where tblVehicle = :num and trackingTime between :satime and :sdtime  ");
			query.setParameter("num", Vehicleid);
			query.setParameter("sdtime", etime);
			query.setParameter("satime", stime);
			cur = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
			
			
		}catch(Exception e){
			
			e.printStackTrace();
		}
		
		
		return cur;
	}
	
	
	
	

}
