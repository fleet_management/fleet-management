package com.roshan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ReportModel {

	
	public List<?> searchAccReport(String cat) {
		
		
		
		List<?> fuel = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			Query query = session.createQuery("from TblMaintaince as tm inner join tm.tblVehicle as tv  where tm.status = :num and tm.comment = :cat ");
			query.setParameter("num", (byte)1);
			query.setParameter("cat", cat);
			fuel = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return fuel;
	}
	
	
	public List searchSummer(Date ftime , Date ttime) {
		
		
		
		List fuel = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			//Query query = session.createQuery("select sum(numOfItems) ,sum(totalPrice) ,itemsId from TblMaintaince  where status = :num and comment = :cat group by itemsId ");
			Query query = session.createQuery("select comment, count(comment) ,sum(totalPrice)  from TblMaintaince  where status = :num and insertDateTime between :fdate and :tdate  group by comment ");
			query.setParameter("num", (byte)1);
			query.setParameter("fdate", ftime);
			query.setParameter("tdate", ttime);
			//query.setParameter("cat", cat);
			fuel = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return fuel;
	}
	
	
	
}
