package com.roshan.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.roshan.Interfaces.FuelManage;
import com.roshan.entity.TblEmployee;
import com.roshan.entity.TblFulePriceHistoty;
import com.roshan.entity.TblFulePriceHistotyId;
import com.roshan.entity.TblFuleType;

public class FuelModel implements FuelManage {

	@Override
	public void addFuel(TblFuleType fuel,TblFulePriceHistoty fuelhisory ,TblFulePriceHistotyId fuelhistoryid ) {
	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(fuel);
			transaction.commit();
			session.close();
			sessionfactory.close();	
			
			SessionFactory sessionfactory1 = new Configuration().configure().buildSessionFactory();
			Session session1 = sessionfactory1.openSession();
			Transaction transaction1 = session1.beginTransaction();
			session1.save(fuelhisory);
			transaction1.commit();
			session1.close();
			sessionfactory1.close();	
			
			}catch(Exception e){
				
				System.out.println(e);
			}
			
		

	}

	@Override
	public void UpdateFuel(TblFuleType fuel,TblFulePriceHistoty fuelhisory) {
		
	try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();				
			session.update(fuel);			
			transaction.commit();
			session.close();
			sessionfactory.close();
			
			
			
			SessionFactory sessionfactory1 = new Configuration().configure().buildSessionFactory();
			Session session1 = sessionfactory1.openSession();
			Transaction transaction1 = session1.beginTransaction();
			session1.save(fuelhisory);
			transaction1.commit();
			session1.close();
			sessionfactory1.close();
				
			}catch(Exception e){
				
				System.out.println(e);
			}

	}

	@Override
	public void deleteFuel(int fuelid) {

		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			TblFuleType employee = (TblFuleType)session.get(TblFuleType.class, fuelid);
			employee.setStatus((byte)3);			
			session.update(employee);		
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){
				
				System.out.println(e);
			}
			

	}

	@Override
	public TblFuleType viewEmployee(int fuelid) {
		TblFuleType fueltype = null;
		try{
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		fueltype = (TblFuleType) session.get(TblFuleType.class, fuelid);
		transaction.commit();
		session.close();
		sessionfactory.close();
			
		}catch(Exception e){
			
			System.out.println(e);
		}
		return fueltype;
	}

	@Override
	public List getAllFuel() {
		
		List fuel = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			Query query = session.createQuery("from TblFuleType where status = :num ");
			query.setParameter("num", (byte)1);
			fuel = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return fuel;
	}

	@Override
	public List searchFuel(String num) {
		
		
		//boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblFuleType where name  LIKE :num and status = :stat");
			query.setParameter("num", "%"+num+"%");
			query.setParameter("stat", (byte)1);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			//if(users.isEmpty()){			
				//result = false;}	
		
		   return users;
	}

	@Override
	public boolean fuelValidate(String num) {
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblFuleType where name = :num and status = :stat ");
			query.setParameter("num", num);
			query.setParameter("stat", (byte)1);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			if(users.isEmpty()){			
				result = false;}	
		
		   return result;
	}
	
	
	public boolean fuelUpdateValidate(String num) {
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblFuleType where name = :num and status = :stat");
			query.setParameter("num", num);
			query.setParameter("stat", (byte)1);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
			if(users.size()==1||users.isEmpty()){
			
				result = false;}	
		
		   return result;
	}


}
