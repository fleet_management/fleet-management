package com.roshan.model;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import com.roshan.Interfaces.EmployeeManage;
import com.roshan.entity.TblEmployee;

public class EmployeeModel implements EmployeeManage {
	
	TblEmployee employee = new TblEmployee();

	@Override
	public void addEmployee(TblEmployee employee) {
		
		try{
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(employee);
		transaction.commit();
		session.close();
		sessionfactory.close();	
		}catch(Exception e){
			
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void UpdateEmployee(TblEmployee employee) {
	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();				
			session.update(employee);			
			transaction.commit();
			session.close();
			sessionfactory.close();
				
			}catch(Exception e){
				
				System.out.println(e);
			}
		
	}

	@Override
	public void deleteEmployee(int employeeid) {
		
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			TblEmployee employee = (TblEmployee)session.get(TblEmployee.class, employeeid);
			employee.setStatus((byte)3);			
			session.update(employee);		
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){
				
				System.out.println(e);
			}
			
		
	}

	@Override
	public TblEmployee viewEmployee(int employeeid) {
		
		TblEmployee employee = null;
			try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			employee = (TblEmployee)session.get(TblEmployee.class, employeeid);
			transaction.commit();
			session.close();
			sessionfactory.close();
				
			}catch(Exception e){
				
				System.out.println(e);
			}
			return employee;
			
	}

	@Override
	public List getAllEmployee() {
		
		List users = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			Query query = session.createQuery("from TblEmployee where status = 1 ");
			users = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return users;
	}
	
	@Override
	public boolean employeeValidate(String num){
		
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblEmployee where employeeNumber = :num and status = 1");
			query.setParameter("num", num);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			if(users.isEmpty()){			
				result = false;}	
		
		   return result;	
	}
	
	public List searchEmployee(String num){
		
		
		//boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblEmployee where employeeNumber LIKE :num and status = 1");
			query.setParameter("num", "%"+num+"%");
			users = query.list();	
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}

		   return users;	
		
		
	}
	
	
	
	public boolean employeeUpdateValidate(String num) {
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblEmployee where employeeNumber = :num and status = 1");
			query.setParameter("num", num);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
			if(users.size()==1||users.isEmpty()){
			
				result = false;}	
		
		   return result;
	}
	
	
	public List RestEmployee(String num){
		
		
		//boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblEmployee where employeeNumber = :num and status = 1");
			query.setParameter("num", num);
			users = query.list();	
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}

		   return users;	
		
		
	}
	
	
	
	

	

}
