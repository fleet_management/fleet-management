package com.roshan.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.roshan.bean.LoginError;
import com.roshan.bean.Success;
import com.roshan.entity.TblEmployee;

public class Login {
	
	String name;
	String password;
	public Login(String name, String password) {
		super();
		this.name = name;
		this.password = password;
	}
	

	public Object result(String name , String  password){
		
		
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblEmployee where employeeNumber = :name and  password = :password");
			query.setParameter("password", password);
			query.setParameter("name", name);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			if(users.isEmpty()){			
				
				LoginError le = new LoginError(name);
				return le;
				
			}else{
					
				TblEmployee employee = (TblEmployee)users.iterator().next();	
				Success sc = new Success(employee.getId(),employee.getRole(),employee.getMstOrganization().getId(),employee.getName(),employee.getEmployeeNumber());
				
				return sc;
			}
			
			
			
		
		   
	}
		
	

}
