package com.roshan.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.roshan.Interfaces.VehicleLogger;
import com.roshan.entity.TblEmployee;
import com.roshan.entity.TblVehivaleLogger;

public class VehicleLoggerModel implements VehicleLogger {

	@Override
	public void addLogger(TblVehivaleLogger logger) {
		try{
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(logger);
		transaction.commit();
		session.close();
		sessionfactory.close();	
		}catch(Exception e){
			
			System.out.println(e);
		}

	}

	@Override
	public void UpdateLogger(TblVehivaleLogger logger) {
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();				
			session.update(logger);			
			transaction.commit();
			session.close();
			sessionfactory.close();
				
			}catch(Exception e){
				
				System.out.println(e);
			}

	}

	@Override
	public void deleteLogger(int loggerid) {
		
			try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			TblVehivaleLogger logger = (TblVehivaleLogger)session.get(TblVehivaleLogger.class, loggerid);
			logger.setStatus((byte)3);			
			session.update(logger);		
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){
				
				System.out.println(e);
			}
	}
		

	@Override
	public TblVehivaleLogger viewLogger(int loggerid) {
		
		TblVehivaleLogger logger = null;
		try{
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		logger = (TblVehivaleLogger)session.get(TblVehivaleLogger.class, loggerid);
		transaction.commit();
		session.close();
		sessionfactory.close();
			
		}catch(Exception e){
			
			System.out.println(e);
		}
		return logger;
	}

	@Override
	public List getAllLogger(String Type) {
		List users = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			Query query = session.createQuery("from TblEmployee where type = :type");
			query.setParameter("type", Type);
			users = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return users;
	}

	@Override
	public List searchLogger(String num) {
		
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblEmployee where employeeNumber = :num");
			query.setParameter("num", num);
			query.setParameter("num", num);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			//if(users.isEmpty()){			
				//result = false;}	
		
		   return users;
	}

	@Override
	public boolean LoggerValidate(String num) {
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblEmployee where employeeNumber = :num");
			query.setParameter("num", num);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			if(users.isEmpty()){			
				result = false;}	
		
		   return result;	
	}

}
