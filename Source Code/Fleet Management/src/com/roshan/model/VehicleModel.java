package com.roshan.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.roshan.Interfaces.VehicleManage;
import com.roshan.entity.TblFuleType;
import com.roshan.entity.TblVehicle;

public class VehicleModel implements VehicleManage {

	@Override
	public void addVehicle(TblVehicle vehicle) {
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(vehicle);
			transaction.commit();
			session.close();
			sessionfactory.close();	
			}catch(Exception e){
				
				System.out.println(e);
			}
	}

	@Override
	public void UpdateVehicle(TblVehicle vehicle) {
		
		try{
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();				
			session.update(vehicle);			
			transaction.commit();
			session.close();
			sessionfactory.close();
				
			}catch(Exception e){
				
				System.out.println(e);
			}
	}

	@Override
	public void deleteVehicle(int vehicleid) {
		
			try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			TblVehicle employee = (TblVehicle)session.get(TblVehicle.class, vehicleid);
			employee.setStatus("3");			
			session.update(employee);		
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){
				
				System.out.println(e);
			}
	}

	@Override
	public TblVehicle viewEmployee(int vehicleid) {
		
		TblVehicle fueltype = null;
		try{
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		fueltype = (TblVehicle) session.get(TblVehicle.class, vehicleid);
		transaction.commit();
		session.close();
		sessionfactory.close();
			
		}catch(Exception e){
			
			System.out.println(e);
		}
		return fueltype;
	}

	@Override
	public List getAllVehicle() {
		
		List users = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			Query query = session.createQuery("from TblVehicle where status = 1 ");
			users = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return users;
	}

	@Override
	public boolean vehicleValidate(String num) {
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblVehicle where registrationNumber = :num");
			query.setParameter("num", num);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			if(users.isEmpty()){			
				result = false;}	
		
		   return result;	
	
	}

	@Override
	public List searchVehicle(String num) {
		
		//boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblVehicle where registrationNumber LIKE :num and status = 1");
			query.setParameter("num", "%"+num+"%");
			//query.setParameter("stat", "1");	
			users = query.list();	
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			//if(users.isEmpty()){			
				//result = false;}	
		
		   return users;
	}
	
	public boolean VehicleUpdateValidate(String num) {
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblVehicle where registrationNumber = :num and status = :stat");
			query.setParameter("num", num);
			query.setParameter("stat", "1");
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
			if(users.size()==1||users.isEmpty()){
			
				result = false;}	
		
		   return result;
	}

}
