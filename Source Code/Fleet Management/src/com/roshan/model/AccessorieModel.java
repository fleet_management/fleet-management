package com.roshan.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.roshan.Interfaces.Accessory;
import com.roshan.entity.TblAccessorie;
import com.roshan.entity.TblAccessoriesPriceHistoty;

public class AccessorieModel implements Accessory {

	@Override
	public void addAccessory(TblAccessorie accessory) {
		
		
			try{
			
				SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
				Session session = sessionfactory.openSession();
				Transaction transaction = session.beginTransaction();
				session.save(accessory);
				transaction.commit();
				session.close();
				sessionfactory.close();	
				
				/*SessionFactory sessionfactory1 = new Configuration().configure().buildSessionFactory();
				Session session1 = sessionfactory1.openSession();
				Transaction transaction1 = session1.beginTransaction();
				session1.save(ah);
				transaction1.commit();
				session1.close();
				sessionfactory1.close();	*/
	
			}catch(Exception e){
				e.printStackTrace();
				//System.out.println(e);
			}

	}

	@Override
	public void UpdateAccessory(TblAccessorie accessory) {
		try{
			
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(accessory);
		transaction.commit();
		session.close();
		sessionfactory.close();	
		
		//SessionFactory sessionfactory1 = new Configuration().configure().buildSessionFactory();
	//	Session session1 = sessionfactory1.openSession();
		//Transaction transaction1 = session1.beginTransaction();
		//session1.save(ah);
		//transaction1.commit();
		//session1.close();
		//sessionfactory1.close();	
		
		}catch(Exception e){
			
			e.printStackTrace();
		}

	}

	@Override
	public void deleteAccessory(int accessoryid) {
	try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			TblAccessorie employee = (TblAccessorie)session.get(TblAccessorie.class, accessoryid);
			employee.setStatus((byte)3);			
			session.update(employee);		
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){
				
				System.out.println(e);
			}
			

	}

	@Override
	public TblAccessorie viewEmployee(int accessoryid) {
		TblAccessorie at = null;
		try{
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		at = (TblAccessorie) session.get(TblAccessorie.class, accessoryid);
		transaction.commit();
		session.close();
		sessionfactory.close();
			
		}catch(Exception e){
			
			System.out.println(e);
		}
		return at;
	}

	@Override
	public List getAllAccessory() {
		List fuel = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		
			Query query = session.createQuery("from TblAccessorie where status = :num ");
			query.setParameter("num", (byte)1);
			fuel = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return fuel;
	}

	@Override
	public List searchAccessory(String num) {
		//boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblAccessorie where name  LIKE :num and status = :stat");
			query.setParameter("num", "%"+num+"%");
			query.setParameter("stat", (byte)1);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			//if(users.isEmpty()){			
				//result = false;}	
		
		   return users;
	}

	@Override
	public boolean AccessoryValidate(String num) {
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblAccessorie where name = :num  ");
			query.setParameter("num", num);
			
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
				
			if(users.isEmpty()){			
				result = false;}	
		
		   return result;
	}
	
	
	
	public boolean fuelUpdateValidate(String num) {
		boolean result = true;	
		List users = new ArrayList();	
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();			
			Query query = session.createQuery("from TblAccessorie where name = :num and status = :stat");
			query.setParameter("num", num);
			query.setParameter("stat", (byte)1);
			users = query.list();			
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){	
				
			System.out.println(e);
			}
			if(users.size()==1||users.isEmpty()){
			
				result = false;}	
		
		   return result;
	}

}
