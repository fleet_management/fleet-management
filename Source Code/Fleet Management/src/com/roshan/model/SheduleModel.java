package com.roshan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.roshan.Interfaces.Shedule;
import com.roshan.entity.TblFuleType;
import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblSchedule;

public class SheduleModel implements Shedule {
	
	
	
	@Override
	public void addMaintain(TblSchedule tm) {
		
		
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(tm);
			transaction.commit();
			session.close();
			sessionfactory.close();	
			

			
			}catch(Exception e){
				
				System.out.println(e);
			}
			

	}

	@Override
	public void UpdateMaintain(TblSchedule tm) {

		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();				
			session.update(tm);			
			transaction.commit();
			session.close();
			sessionfactory.close();
			
				
			}catch(Exception e){
				
				System.out.println(e);
			}


	}


	@Override
	public void deleteShedule(int mid) {
		
	try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();
			TblSchedule maintains = (TblSchedule)session.get(TblSchedule.class, mid);
			maintains.setStatus((byte)3);			
			session.update(maintains);		
			transaction.commit();
			session.close();
			sessionfactory.close();
	
			}catch(Exception e){
				
				System.out.println(e);
			}
	}

	@Override
	public List viewMaintain(int mid) {
		List<?> shedule = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		 
			Query query = session.createQuery("from TblSchedule c inner join c.tblVehicles inner join c.tblEmployees where c.id = :cat");
			//query.setParameter("num", (byte)1);
			query.setParameter("cat", mid);
			shedule = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return shedule;
	}

	@Override
	public List getAllShedule() {
		List<?> shedule = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		 
			Query query = session.createQuery("from TblSchedule c inner join c.tblVehicles where c.status = :num ");
			query.setParameter("num", (byte)1);
			//query.setParameter("cat", cat);
			shedule = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return shedule;
	}
	
	
	
	public List SelectSedule(Date sdtime , Date satime){
		
		System.out.println(sdtime);
		System.out.println(satime);
		
		List<?> shedule = new ArrayList();
		try{
			
			SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionfactory.openSession();
			Transaction transaction = session.beginTransaction();		 
			Query query = session.createQuery("from TblSchedule c inner join c.tblVehicles where c.status = :num  and c.startDateTime between :satime and :sdtime ");
			query.setParameter("num", (byte)1);
			query.setParameter("sdtime", sdtime);
			query.setParameter("satime", satime);
			//query.setParameter("cat", cat);
			shedule = query.list();			
			transaction.commit();
			session.close();  
			sessionfactory.close();
	
			}catch(Exception e){				
				System.out.println(e);
			}
		
			return shedule;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public List searchShedule(String vnum) {
		
		List users = new ArrayList();	
		
			
			
			
			try{
				
				SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
				Session session = sessionfactory.openSession();
				Transaction transaction = session.beginTransaction();			
				Query query = session.createQuery("from TblSchedule c inner join c.tblVehicles v where c.status = :num  and v.registrationNumber LIKE :vnum");
				query.setParameter("vnum", "%"+vnum+"%");
				query.setParameter("num", (byte)1);
				//query.setParameter("cat", cat);
				users = query.list();			
				transaction.commit();
				session.close();
				sessionfactory.close();
		
				}catch(Exception e){	
					
				System.out.println(e);
				}
					

			
	
		

		   return users;
	}

	@Override
	public boolean MaintainValidate(int vnum, Date sdate, Date edate) {
		// TODO Auto-generated method stub
		return false;
	}


}
