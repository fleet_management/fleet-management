package com.roshan.entity;
// Generated Jun 17, 2016 4:39:31 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TblActivityLogger generated by hbm2java
 */
@Entity
@Table(name = "tbl_activity_logger", catalog = "fleet")
public class TblActivityLogger implements java.io.Serializable {

	private int id;

	public TblActivityLogger() {
	}

	public TblActivityLogger(int id) {
		this.id = id;
	}

	@Id

	@Column(name = "ID", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
