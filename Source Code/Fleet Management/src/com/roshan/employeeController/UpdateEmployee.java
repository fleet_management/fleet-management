package com.roshan.employeeController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.MstOrganization;
import com.roshan.entity.TblEmployee;
import com.roshan.ids.Ids;
import com.roshan.model.EmployeeModel;

/**
 * Servlet implementation class UpdateEmployee
 */
@WebServlet("/pages/employee/UpdateEmployee")
public class UpdateEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	EmployeeModel em = new EmployeeModel();
	TblEmployee employee = new TblEmployee();
	MstOrganization org = new MstOrganization();
	EmployeeModel emp = new EmployeeModel();
	boolean result;
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	
	
    public UpdateEmployee() {
        super();
       
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		org.setId("1");
		
		String employee_name =null,employee_num=null,permenet_address=null,tempory_address=null,nic=null,telephone=null;
		String mobile1 =null , mobile2 =null,gender = null,fax=null,licence_number=null,web=null,epf_number=null,role=null;
		
		try {
			 employee_name = request.getParameter("employee_name");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			employee_num  = request.getParameter("employee_num");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 permenet_address  = request.getParameter("permenet_address");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 tempory_address  = request.getParameter("tempory_address");
			} catch (Exception e) {
				System.out.println(e);
			}
		
		try {
			 nic  = request.getParameter("nic");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 telephone  = request.getParameter("telephone");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
		 mobile1  = request.getParameter("mobile1");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
		 mobile2  = request.getParameter("mobile2");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
		 gender  = request.getParameter("gender");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		try {
		 fax  = request.getParameter("fax");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		
		try {
		 licence_number  = request.getParameter("licence_number");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		
		try {
		 epf_number  = request.getParameter("epf_number");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		try {
		 role  = request.getParameter("role");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
			try {
			 web  = request.getParameter("web");
			} catch (Exception e) {
				System.out.println(e);
			}
		
		
		Date licence_ex_date =null,bday=null;
		try {
			 licence_ex_date  = formatter.parse(request.getParameter("licence_ex_date"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		try {
			bday  = formatter.parse(request.getParameter("bday"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		double salary=0;
		
		try {
			salary = Double.parseDouble(request.getParameter("salary"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		int id=0;
		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		result = em.employeeUpdateValidate(employee_num);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("isValid", result);
			
		
			
		if(!result){
				
				employee.setId(id);
				
				employee.setName(employee_name);
				employee.setEmployeeNumber(employee_num);
				employee.setPermenetAddress(permenet_address);
				employee.setTemporyAddress(tempory_address);
				employee.setMobile(mobile1);
				employee.setMobile2(mobile2);
				employee.setBday(bday);
				employee.setGender(gender);
				employee.setFax(fax);
				employee.setWeb(web);
				employee.setLicenceExpireDate(licence_ex_date);
				employee.setNic(nic);
				employee.setTelephone(telephone);
				employee.setLicenceNum(licence_number);
				employee.setEpfNumber(epf_number);
				employee.setSalary(salary);
				employee.setPassword(Integer.toString(Ids.getId()));
				employee.setRole(role);
				
				employee.setStatus((byte)1);
				employee.setMstOrganization(org);
				
				
				emp.UpdateEmployee(employee);

				
			}
				

			 write(response,map);
		
	}
	
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}


}
