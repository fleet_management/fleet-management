package com.roshan.employeeController;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.roshan.bean.Success;
import com.roshan.model.Login;

/**
 * Servlet implementation class LoginForm
 */
@WebServlet("/pages/login/LoginForm")
public class LoginForm extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Map<String,Object> map = new HashMap<String,Object>();

    public LoginForm() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		
		String name = request.getParameter("userName");
		String password = request.getParameter("password");
		
		MessageDigest md;
		String hpass = null;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes(),0,password.length());
			
			hpass = new BigInteger(1,md.digest()).toString();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(hpass);
		
		Login login = new Login(name,hpass);
		Object obj =login.result(name,hpass);
			
		
		if(obj.getClass().toString().compareTo("class com.roshan.bean.LoginError")==0){
			
			response.sendRedirect("loginform.jsp?q=1");
			//boolean bool = false;
			//map.put("isValid", bool);
			//write(response,map);
			
			//System.out.println("hell");
			
		}else{
			
		
			 Success sc = (Success)obj;
			 HttpSession session = request.getSession();

			 session.setAttribute("id", sc.getId());
			 session.setAttribute("UserName", sc.getUserName());
			 session.setAttribute("type", sc.getType());
			 session.setAttribute("userid", sc.getUserID());
			 session.setAttribute("orgid", sc.getOrgid());
			// System.out.println("hell jj");
			 
			 if( sc.getType().compareTo("super_admin")==0){
				 
				 System.out.println("hellow");
				 
				 response.sendRedirect("../dashboard/Dashboard.jsp"); 
				 
			 }else{
				 
				 response.sendRedirect("loginform.jsp?q=2");
					//boolean bool = false;
					//map.put("isValid", bool);
					//write(response,map);
			 }
			 
			 
			
		}
		
		

	
		}


		private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
	

				response.setContentType("json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(new Gson().toJson(map));

		}
}
