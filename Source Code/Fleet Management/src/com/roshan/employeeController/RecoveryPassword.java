package com.roshan.employeeController;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.MstOrganization;
import com.roshan.entity.TblEmployee;
import com.roshan.ids.Ids;
import com.roshan.model.EmployeeModel;

/**
 * Servlet implementation class RecoveryPassword
 */
@WebServlet("/pages/login/RecoveryPassword")
public class RecoveryPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecoveryPassword() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String name = request.getParameter("userName");
		
		EmployeeModel em = new EmployeeModel();
		List <TblEmployee>list =  em.RestEmployee(name);
		
		
		
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		System.out.println(name);
		if( list.size()!=0){
			
			
			
			Test t = new Test();
			
			    TblEmployee te =   (TblEmployee)list.get(0);
			    String s[] = {te.getMobile2()};
			    
			    
			    MessageDigest md;
				String pass = Integer.toHexString((Ids.getId()));
				String hpass = null;
				try {
					md = MessageDigest.getInstance("MD5");
					md.update(pass.getBytes(),0,pass.length());
					
					hpass = new BigInteger(1,md.digest()).toString();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			TblEmployee tee = new TblEmployee();
		
			
			
			
			tee.setName(te.getName());
			tee.setEmployeeNumber(te.getEmployeeNumber());
			tee.setPermenetAddress(te.getPermenetAddress());
			tee.setTemporyAddress(te.getTemporyAddress());
			tee.setMobile(te.getMobile());
			tee.setMobile2(te.getMobile2());
			tee.setBday(te.getBday());
			tee.setGender(te.getGender());
			tee.setFax(te.getFax());
			tee.setWeb(te.getWeb());
			tee.setLicenceExpireDate(te.getLicenceExpireDate());
			tee.setNic(te.getNic());
			tee.setTelephone(te.getTelephone());
			tee.setLicenceNum(te.getLicenceNum());
			tee.setEpfNumber(te.getEpfNumber());
			tee.setSalary(te.getSalary());
			tee.setPassword(hpass);
			tee.setRole(te.getRole());
			tee.setStatus(te.getStatus());
			tee.setId(te.getId());
			tee.setMstOrganization(te.getMstOrganization());
			
			
			
			em.UpdateEmployee(tee);
			    
			    
			t.getdata(s,"Your password :-"+pass);
			
			String s1="Your Password Successfully Sent <a href='loginform.jsp'> Back to Login Page </a>";
			out.println(s1);
		}else{
			
			String s1="Your Email Not Match";
			out.println(s1);
		}
		
		

		
	}

}
