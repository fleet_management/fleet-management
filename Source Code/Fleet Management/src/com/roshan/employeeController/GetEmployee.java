package com.roshan.employeeController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roshan.entity.TblEmployee;
import com.roshan.model.EmployeeModel;

/**
 * Servlet implementation class GetEmployee
 */
@WebServlet("/pages/employee/GetEmployee")
public class GetEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public GetEmployee() {
        super();
     
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		Map<String,Object> map = new HashMap<String,Object>();
		int id = Integer.parseInt(request.getParameter("id"));
		
		EmployeeModel em = new EmployeeModel();
		TblEmployee te = new TblEmployee();
		
		te = em.viewEmployee(id);
		
		
		map.put("employee_name", te.getName());
		map.put("employee_num", te.getEmployeeNumber());
		map.put("permenet_address", te.getPermenetAddress());
		map.put("tempory_address",te.getTemporyAddress());
		map.put("nic", te.getNic());
		map.put("telephone", te.getTelephone());
		map.put("mobile1", te.getMobile());
		map.put("mobile2", te.getMobile2());
		
		try{
		map.put("bday", te.getBday().toString().substring(0, 10));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		map.put("gender", te.getGender());
		map.put("fax", te.getFax());
		map.put("web",te.getWeb());
		map.put("licence_number", te.getLicenceNum());
		
		try{
		map.put("licence_ex_date", te.getLicenceExpireDate().toString().substring(0, 10));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		map.put("epf_number",te.getEpfNumber());
		map.put("salary", te.getSalary());
		map.put("role", te.getRole());

		
		
		
		write(response,map);
		
	}
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}

}
