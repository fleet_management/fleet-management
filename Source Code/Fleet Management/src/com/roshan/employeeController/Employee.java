package com.roshan.employeeController;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblEmployee;
import com.roshan.ids.Ids;
import com.roshan.model.EmployeeModel;
import com.google.gson.Gson;
import com.roshan.entity.MstOrganization;

/**
 * Servlet implementation class EmployeeValidate
 */
@WebServlet("/pages/employee/Employee")
public class Employee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	EmployeeModel em = new EmployeeModel();
	TblEmployee employee = new TblEmployee();
	MstOrganization org = new MstOrganization();
	EmployeeModel emp = new EmployeeModel();
	boolean result;
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	
	
    public Employee() {
        super();
       
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		org.setId("1");
		
		String employee_name =null,employee_num=null,permenet_address=null,tempory_address=null,nic=null,telephone=null;
		String mobile1 =null , mobile2 =null,gender = null,fax=null,licence_number=null,web=null,epf_number=null,role=null;
		
		try {
			 employee_name = request.getParameter("employee_name");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			employee_num  = request.getParameter("employee_num");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 permenet_address  = request.getParameter("permenet_address");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 tempory_address  = request.getParameter("tempory_address");
			} catch (Exception e) {
				System.out.println(e);
			}
		
		try {
			 nic  = request.getParameter("nic");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			 telephone  = request.getParameter("telephone");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
		 mobile1  = request.getParameter("mobile1");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
		 mobile2  = request.getParameter("mobile2");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
		 gender  = request.getParameter("gende");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		try {
		 fax  = request.getParameter("fax");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		
		try {
		 licence_number  = request.getParameter("licence_number");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		
		try {
		 epf_number  = request.getParameter("epf_number");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		try {
		 role  = request.getParameter("role");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
			try {
			 web  = request.getParameter("web");
			} catch (Exception e) {
				System.out.println(e);
			}
		
		
		Date licence_ex_date =null,bday=null;
		try {
			 licence_ex_date  = formatter.parse(request.getParameter("licence_ex_date"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		try {
			bday  = formatter.parse(request.getParameter("bday"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		double salary=0;
		
		try {
			salary = Double.parseDouble(request.getParameter("salary"));
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		try{
			
	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		result = em.employeeValidate(employee_num);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("isValid", result);
			
		
			
		if(!result){
				
				employee.setId(Ids.getId());
				
				employee.setName(employee_name);
				employee.setEmployeeNumber(employee_num);
				employee.setPermenetAddress(permenet_address);
				employee.setTemporyAddress(tempory_address);
				employee.setMobile(mobile1);
				employee.setMobile2(mobile2);
				employee.setBday(bday);
				employee.setGender(gender);
				employee.setFax(fax);
				employee.setWeb(web);
				employee.setLicenceExpireDate(licence_ex_date);
				employee.setNic(nic);
				employee.setTelephone(telephone);
				employee.setLicenceNum(licence_number);
				employee.setEpfNumber(epf_number);
				employee.setSalary(salary);
				
				MessageDigest md;
				String pass = Integer.toHexString((Ids.getId()));
				String hpass = null;
				try {
					md = MessageDigest.getInstance("MD5");
					md.update(pass.getBytes(),0,pass.length());
					
					hpass = new BigInteger(1,md.digest()).toString();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				employee.setPassword(hpass);
				employee.setRole(role);
				
				employee.setStatus((byte)1);
				employee.setMstOrganization(org);
				
				
				emp.addEmployee(employee);
				
				
				//EmailSende es = new EmailSende();
				
				String s[] = {mobile2};
				Test t = new Test();
				t.getdata(s,"Your password :-"+pass);
				//es.sendFromGMail("dmrmanjula@gmail.com", "kavindaya",s, "Login Password for fleet Management System", "Your password :-"+pass);

				
			}
				

			 write(response,map);
		
	}
	
	
	private void write(HttpServletResponse response, Map<String, Object> map)  throws IOException{
		

		response.setContentType("json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	
	}


}
