package com.roshan.employeeController;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.roshan.entity.TblEmployee;
import com.roshan.model.EmployeeModel;

/**
 * Servlet implementation class EmployeeSearch
 */
@WebServlet("/pages/employee/EmployeeSearch")
public class EmployeeSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	EmployeeModel em = new EmployeeModel();
	List<TblEmployee> l;
	
    public EmployeeSearch() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String word = request.getParameter("word");
		if(word.compareTo("NULL")==0){
			
			l= em.getAllEmployee();
		}else{
			l= em.searchEmployee(word);
		}

			write(l, response);
		
		
		
	}
	
	public void write(List<TblEmployee> l,HttpServletResponse response) throws IOException{
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String s="";
		int i = 1;
		for(TblEmployee te : l){
			
			s=s+"<tr><td >"+ i+"</td><td>"+te.getEmployeeNumber()+"</td><td>"+ te.getPermenetAddress()+"</td><td>"+ te.getMobile()+"</td><td>  <a href='UpdateEmployee.jsp?q="+te.getId()+"' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-exclamation-sign cus' aria-hidden='true'></span>Update</a>  <a onclick ='deleteEmployee("+te.getId()+")'class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove cus' aria-hidden='true'></span>Delete</a></td></tr>";
			i++;
			
		}
		out.println(s);

	
	}

}
