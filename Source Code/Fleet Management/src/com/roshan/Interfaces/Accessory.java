package com.roshan.Interfaces;

import java.util.List;

import com.roshan.entity.TblAccessorie;
import com.roshan.entity.TblAccessoriesPriceHistoty;


public interface Accessory {

	
	public void addAccessory(TblAccessorie accessory);
	public void UpdateAccessory(TblAccessorie accessory);
	public void deleteAccessory(int accessoryid);
	public TblAccessorie viewEmployee(int accessoryid);
	public List getAllAccessory();	

	public List searchAccessory(String num);
	public boolean AccessoryValidate(String num);
	
}
