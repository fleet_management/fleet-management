package com.roshan.Interfaces;
import java.util.List;
import com.roshan.entity.TblVehicle;

public interface VehicleManage {
	
	public void addVehicle(TblVehicle vehicle);
	public void UpdateVehicle(TblVehicle vehicle);
	public void deleteVehicle(int vehicleid);
	public TblVehicle viewEmployee(int vehicleid);
	public List getAllVehicle();	
	//public boolean employeeVehicle(String num);
	public List searchVehicle(String num);
	public boolean vehicleValidate(String num);

}
