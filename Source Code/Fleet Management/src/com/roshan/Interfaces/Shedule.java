package com.roshan.Interfaces;

import java.util.Date;
import java.util.List;

import com.roshan.entity.TblSchedule;



public interface Shedule {

	
	public void deleteShedule(int mid);
	public List  viewMaintain(int mid);
	public List getAllShedule();	

	public List searchShedule(String vnum);
	public boolean MaintainValidate(int vnum,Date sdate,Date edate);
	public void addMaintain(TblSchedule tm);
	void UpdateMaintain(TblSchedule tm);
	
}
