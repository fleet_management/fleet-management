package com.roshan.Interfaces;

import java.util.List;

import com.roshan.entity.TblFulePriceHistoty;
import com.roshan.entity.TblFulePriceHistotyId;
import com.roshan.entity.TblFuleType;


public interface FuelManage {

	

	public void deleteFuel(int fuelid);
	public TblFuleType  viewEmployee(int fuelid);
	public List getAllFuel();	

	public List searchFuel(String num);
	public boolean fuelValidate(String num);
	public void addFuel(TblFuleType fuel, TblFulePriceHistoty fuelhisory, TblFulePriceHistotyId fuelhistoryid);
	void UpdateFuel(TblFuleType fuel, TblFulePriceHistoty fuelhisory);
	
}
