package com.roshan.Interfaces;

import java.util.List;

import com.roshan.entity.TblEmployee;

public interface EmployeeManage {

	
	public void addEmployee(TblEmployee employee);
	public void UpdateEmployee(TblEmployee employee);
	public void deleteEmployee(int employeeid);
	public TblEmployee viewEmployee(int employeeid);
	public List getAllEmployee();	
	public boolean employeeValidate(String num);
	public List searchEmployee(String num);
	
}
