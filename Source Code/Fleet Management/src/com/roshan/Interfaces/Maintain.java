package com.roshan.Interfaces;

import java.util.List;


import com.roshan.entity.TblMaintaince;
import com.roshan.entity.TblMaintainceId;

public interface Maintain {
	
	
	public void deleteMaintain(TblMaintainceId mid);
	public TblMaintaince  viewMaintain(TblMaintainceId mid);
	public List getAllMaintain(String cat);	

	public List searchMaintain(String num,String cat);
	public boolean MaintainValidate(String num);
	public void addMaintain(TblMaintaince tm);
	void UpdateMaintain(TblMaintaince tm);

}
