<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body>
<div class="container" >
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content" >
										
<!--- Container-------->


    
    <div class="panel panel-default panel_custom">
  <div class="panel-body">

  	 <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="#">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>MAINTAINS</h3>
  </div>

  </div>
  </div>



  <div class="row">


    <div class="col-md-3">
      <div class="panel panel-default text-center">
        <div class="panel-heading" style="background:  #5bc0de; color: #fff;">
          <h4>FUEL </h4>
        </div>
        <div class="panel-body" >
          Fuel Maintains
        </div>
        <div class="panel-footer" style="background: #fff; border-top: 0px;">
          <a href="FuelMaintain.jsp"><button type="button" class="btn btn-default btn-sm">Click Here</button></a>
        </div>
      </div>
    </div>






    <div class="col-md-3">
      <div class="panel panel-default text-center">
        <div class="panel-heading"style="background: #428bca; color: #fff;">
          <h4>ACCESSORY</h4>
        </div>
        <div class="panel-body">
          Accessory Maintains
        </div>
        <div class="panel-footer" style="background: #fff; border-top: 0px;">
           <a href="AccessoryMaintain.jsp"><button type="button" class="btn btn-default btn-sm">Click Here</button></a>
        </div>
      </div>
    </div>
        <div class="col-md-3">
      <div class="panel panel-default text-center">
        <div class="panel-heading" style="background: #5cb85c; color: #fff;">
          <h4>ACCEDENT</h4>
        </div>
        <div class="panel-body">
           Accedent Maintains
        </div>
        <div class="panel-footer" style="background: #fff; border-top: 0px;">
           <a href="AccedentMaintain.jsp"><button type="button" class="btn btn-default btn-sm">Click Here</button></a>
        </div>
      </div>
    </div>
    
    
    
    
            <div class="col-md-3">
      <div class="panel panel-default text-center">
        <div class="panel-heading" style="background: #18bc9c; color: #fff;">
          <h4>INSUARANCE</h4>
        </div>
        <div class="panel-body">
          Insuarance Maintains
        </div>
        <div class="panel-footer" style="background: #fff; border-top: 0px;">
           <a href="InsuaranceMaintain.jsp"><button type="button" class="btn btn-default btn-sm">Click Here</button></a>
        </div>
      </div>
    </div>
    
    
    </div>
    
    
    
    

    
    <div class="row">



  
    <div class="col-md-3">
      <div class="panel panel-default text-center">
        <div class="panel-heading"style="background: #f0ad4e; color: #fff;">
          <h4>LICENCE UPDATE</h4>
        </div>
        <div class="panel-body">
         Licence Maintains
        </div>
        <div class="panel-footer" style="background: #fff; border-top: 0px;">
           <a href="LicenceMaintain.jsp"><button type="button" class="btn btn-default btn-sm">Click Here</button></a>
        </div>
      </div>
    </div>


    
  </div>










 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
