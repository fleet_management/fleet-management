<%@page import="com.roshan.entity.TblAccessorie"%>
<%@page import="com.roshan.entity.TblVehicle"%>
<%@page import="com.roshan.model.VehicleModel"%>
<%@page import="java.util.List"%>
<%@page import="com.roshan.model.AccessorieModel"%>
<div >

<form class="form-horizontal" method="post" id="acce_from">
 <%
 
AccessorieModel am = new AccessorieModel();
 List <	TblAccessorie> list = am.getAllAccessory();
 
 VehicleModel vm = new VehicleModel();
 List <TblVehicle> vlist = vm.getAllVehicle();
 
 
 %>
 
				
				<div class="form-group" id="form_static">
				    <label for="idate" class="col-sm-2 control-label">Date </label>
				    <div class="col-sm-2">
				    	<input type="date" class="form-control input-sm" id="idate" name="idate">
				    </div>
		
			  
			
				    <label for="vnum" class="col-sm-2 control-label"> Vehicle Number</label>
				    <div class="col-sm-2">
				    
					    <select class="form-control input-sm" id="vnum" name="vnum">   
					   	<option> Select a Vehicle</option>
					     	<%for(TblVehicle te : vlist){ %>
					    	<option value="<%= te.getId()%>"><%= te.getRegistrationNumber()%></option>
					    	<%} %>
					    </select>
					  
				    
				    </div>
				    
				    <label for="vender" class="col-sm-1 control-label">Vender </label>
				    <div class="col-sm-2">
				    	<input type="text" class="form-control input-sm" id="vender" name="vender">
				    </div>
			
			  
			  
			  </div>
			  			  <input type="hidden"  id="id" name="id">
			   <input type="hidden"  id="cat" name="cat" value="accessory">
			  			
				<div class="form-group " id="form_dynamic">
				    
				        <label for="aname" class="col-sm-2 control-label"> Accessory Name</label>
				    <div class="col-sm-4" id="acc_name">
				    
					    <select class="form-control input-sm"  input-sm" id="ftype" name="ftype" onchange="accechange(this.value)">   
					    <option> Select a Accessory</option>
					     	<%for(	TblAccessorie te : list){ %>
					    	<option value="<%= te.getId()%>"><%= te.getName()%></option>
					    	<%} %>
					    </select>
				    
				    </div>
				    
				    
				 
				    
		
				    <label for="uprice" class="col-sm-1 control-label">Unit Price </label>
				    <div class="col-sm-2">
				    	<input type="text" class="form-control input-sm" id="uprice" name="uprice">
				    </div>
		
			  
		
				    
				

				
 		
 		  </div>
 		  
 		  
 		  <div class="form-group" id="form_dynamic">
 		  
 		  			   <label for="nunit" class="col-sm-2 control-label">Num Of Units </label>
				    <div class="col-sm-1">
				   		 <input type="text" class="form-control input-sm" id="nunit" name="nunit" onkeyup="calSubTotal(this.value);">
				    </div>
 		  
 		  	    <label for="stotal" class="col-sm-1 control-label">Subtotal </label>
				    <div class="col-sm-2">
				   		 <input type="text" class="form-control input-sm" id="stotal" name="stotal">
				    </div>
				    
				      <label for="discount" class="col-sm-1 control-label">Discount % </label>
				    <div class="col-sm-1">
				   		 <input type="text" class="form-control input-sm" id="discount" name="discount" onkeyup="caldiscount(this.value);">
				    </div>
				    
				  <label for="tvalue" class="col-sm-1 control-label" >Total Value </label>
				    <div class="col-sm-2">
				   		 <input type="text" class="form-control input-sm" id="tvalue" name="tvalue">
				    </div>
 		  
 		  
 		  
 		  </div>
 		  	 <div class="btn_save_clear">
 		
 		<button type="reset" class="btn btn-danger" >Reset</button>
  		<button type="submit" class="btn btn-success">Save</button>
 		</div>
 		
  
  
</form>


</div>
	





