<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body onload="readMaintainRecords('accedent','#fmaccedent','GetFuel')">
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="maintain.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>ACCEDENT</h3>
  </div>
    
  

   <div style="clear:both"></div>
        <div id="fradde" class="alert alert-danger hidden">
          <span class="glyphicon glyphicon-star"></span> Accedent Record Alredy Exits
        </div>
       
         <div id="fradds" class="alert alert-success hidden">
          <span class="glyphicon glyphicon-star"></span> Accedent Record Added Sucssefully
        </div>
	
	

	

	
	
    
  </div>
</div>


<!-- --End Upper Panel -->
<!-- -Start Form -->

<div class="panel panel-default" id="internal_form">
  <div class="panel-body">
  
  	<jsp:include page="AccedentForms.jsp"/>
  
  </div>
  </div>

<!-- -End Form -->

<div class="panel panel-default">
  <div class="panel-body">

  <div class="row">

  		 <div class="col-md-6">
  		 	
					<div class="input-group" id="search_bar" >
                      <input type="text" name="message" placeholder="Search Here..." class="form-control" onkeyup="searchmaintaina(this.value,'accedent','#fmaccedent');">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-flat">Search</button>
                      </span>
                   </div>

  		 </div>
  		 <div class="col-md-4">

  		 </div>
  <div class="col-md-2">
  		






  </div>                 
	</div>

   
</div>
<div class="row tbl_border">
<table class="table table-bordered table-hover tbl_cus">
 

     


<thead>
           <tr>
           	<th class="col-xs-1">No</th>
             <th class="col-xs-2">Vehicle</th>
             <th class="col-xs-2">Accedent Date</th>
             <th class="col-xs-2">Accedent Cost</th>
             <th class="col-xs-3">Action</th>
           </tr>
</thead>

		<tbody id="fmaccedent">
		
		
		
		</tbody>

		</table>
	</div>
	

	
	
	
	
  </div>


 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
