<%@page import="com.roshan.model.FuelModel"%>
<%@page import="com.roshan.entity.TblFuleType"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body onload="readFuelTypeRecords()">
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="FuelMaintain.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>FUEL MANAGE</h3>
  </div>
    
  

  <div style="clear:both"></div>
    
    
    
  
    
    
    
  </div>
</div>


<!-- --End Upper Panel -->




<div class="panel panel-default">
  <div class="panel-body">

  <div class="row">

  		 <div class="col-md-6">
  		 	
					<div class="input-group" id="search_bar" >
                      <input type="text" name="message" placeholder="Search Here..." class="form-control" onkeyup=" searchfueltype(this.value);">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-flat">Search</button>
                      </span>
                   </div>

  		 </div>
  		 <div class="col-md-4">

  		 </div>
  <div class="col-md-2">
  		<a   class="btn btn-success " data-toggle="modal" data-target=".add_new" onclick="resetmodel();">New Fuel</a> 


<div class="modal fade add_new" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    
      <div class="modal-body">
       
       
       
       
       
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>  


<div class="panel panel-default">
  <div class="panel-heading">New Fuel</div>
  <div class="panel-body">
    			
      <div id="fadde" class="alert alert-danger hidden">
          <span class="glyphicon glyphicon-star"></span> Fuel Type Alredy Exits
        </div>
       
         <div id="fadds" class="alert alert-success hidden">
          <span class="glyphicon glyphicon-star"></span> Fuel Type Added Sucssefully
        </div>
       
       		<form class="form-horizontal" id="fuel_type">
       	
       	  <div class="form-group">
    <label for="fuel_name" class="col-sm-3 control-label"><span class="acsil">*</span>Name</label>
    <div class="col-sm-8">
    <input type="text" class="form-control input-sm" id="fuel_name" name="fuel_name">
    </div>
  </div>
  

<input type="hidden" id="id" name="id">
  
      <div class="form-group">
    <label for="price" class="col-sm-3 control-label"><span>*</span>Price </label>
    <div class="col-sm-7">
    <input type="text" class="form-control input-sm" id="price" name="price">
    </div>
  </div>
       
       
       <div class="btn_save_clear">
 		
 		<button type="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
  		<button type="submit" class="btn btn-success">Save</button>
 		</div>
 		</form>
       

</div>
</div>  
       
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




  </div>                 
	</div>

   
</div>
<div class="row tbl_border">
<table class="table table-bordered table-hover tbl_cus">
 

  <%

	FuelModel fm = new FuelModel();
		List<TblFuleType> list = fm.getAllFuel();
%>
     


<thead>
           <tr>
           	<th class="col-xs-1">No</th>
             <th class="col-xs-3">Fuel Type</th>
             <th class="col-xs-3">Price</th>
             <th class="col-xs-3">Action</th>
           </tr>
</thead>
<tbody id="tbl_fuelType">

</tbody>
		</table>
	</div>
	
	
	<div>
		
	
	
	</div>
	
	
	
	
	
  </div>


 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
