<%@page import="com.roshan.entity.TblVehicle"%>
<%@page import="java.util.List"%>
<%@page import="com.roshan.model.VehicleModel"%>


<div>
	
 <%
 
 
 VehicleModel vm = new VehicleModel();
 List <TblVehicle> vlist = vm.getAllVehicle();
 
 
 %>
	
<form class="form-horizontal" id="licencefrom" >


			  			
				<div class="form-group" >
				    
			
				    <div class="col-sm-2">
				      <h6> Vehicle Number </h6>
					    <select class="form-control input-sm" id="vnum" name="vnum" required>   
					     <option> Select a Vehicle</option>
					     	<%for(TblVehicle te : vlist){ %>
					    	<option value="<%= te.getId()%>"><%= te.getRegistrationNumber()%></option>
					    	<%} %>
					    </select>
				    
				    </div>
				    
				    
				   
				    <div class="col-sm-3">
				    <h6> Licenced Date </h6>
				    	<input type="date" class="form-control input-sm" id="idate" name="idate" required>
				    </div>
		
				    
				      
				    <div class="col-sm-3">
				      <h6> Expire Date </h6>
				   		 <input type="date" class="form-control input-sm" id="edate" name="edate" required>
				    </div>
				    
				 
				    <div class="col-sm-2">
				      <h6> Licence Cost </h6>
				   		 <input type="text" class="form-control input-sm" id="tvalue" name="tvalue" required>
				    </div>
			
	

  
				<input type="hidden"  id="id" name="id">
			   <input type="hidden"  id="cat" name="cat" value="licence">
  	
 		
 		  </div>
 		  
 		  	 <div class="btn_save_clear">
 		
 		<button type="reset" class="btn btn-danger">reset</button>
  		<button type="submit" class="btn btn-success">Save</button>
 		</div>
 		
  
  
</form>


</div>
	





