$(document).ready(function () {
	
	
		var validator = $("#fuel_type").bootstrapValidator({
			feedbackIcons: {
				valid: "glyphicon glyphicon-ok",
				invalid: "glyphicon glyphicon-remove", 
				validating: "glyphicon glyphicon-refresh"
			}, 
			fields : {
				fuel_name :{
					validators : {
						notEmpty : {
							message : "Vehicle Number is required"
						}
					}
				}, 
				price : {
					validators: {
						notEmpty : {
							message : "Model is required"
						}
					}
				}
			}
		});
	
		validator.on("success.form.bv", function (e) {
			e.preventDefault();
			fuelAdd();
			//$("#registration-form").addClass("hidden");
			//$("#confirmation").removeClass("hidden");
		});
		










//////////////////////////////////////////////////////////////////////////////////////////////////////////////////








	
	
	var validator = $("#new_Accessory").bootstrapValidator({
		feedbackIcons: {
			valid: "glyphicon glyphicon-ok",
			invalid: "glyphicon glyphicon-remove", 
			validating: "glyphicon glyphicon-refresh"
		}, 
		fields : {
			id :{
				validators : {
					notEmpty : {
						message : "Accessory id is required"
					}
				}
			}, 
			price : {
				validators: {
					notEmpty : {
						message : "Price is required"
					}
				}
			}
		}
	});

	validator.on("success.form.bv", function (e) {
		e.preventDefault();
		AccessoryAdd();
		//$("#registration-form").addClass("hidden");
		//$("#confirmation").removeClass("hidden");
	});
	
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	var validator = $("#fuelfrom").bootstrapValidator({
		feedbackIcons: {
		
		}, 
		fields : {
			idate :{
				validators : {
					notEmpty : {
						message : "date is required"
					}
				}
			}, 
			ftype : {
				validators: {
					notEmpty : {
						message : "Fuel is required"
					}
				}
			},
			vnum : {
				validators: {
					notEmpty : {
						message : "Vehicle is required"
					}
				}
			},
			nunit : {
				validators: {
					notEmpty : {
						message : "Units is required"
					}
				}
			},
			discount : {
				validators: {
					notEmpty : {
						message : "Discount is required"
					}
				}
			}
		}
	});

	validator.on("success.form.bv", function (e) {
		e.preventDefault();
		fuelformadd("#fuelfrom");
		//$("#registration-form").addClass("hidden");
		//$("#confirmation").removeClass("hidden");
	});
	

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	var validator = $("#acce_from").bootstrapValidator({
		feedbackIcons: {
		
		}, 
		fields : {
			idate :{
				validators : {
					notEmpty : {
						message : "date is required"
					}
				}
			}, 
			ftype : {
				validators: {
					notEmpty : {
						message : "Fuel is required"
					}
				}
			},
			vnum : {
				validators: {
					notEmpty : {
						message : "Vehicle is required"
					}
				}
			},
			nunit : {
				validators: {
					notEmpty : {
						message : "Units is required"
					}
				}
			},
			discount : {
				validators: {
					notEmpty : {
						message : "Discount is required"
					}
				}
			}
		}
	});

	validator.on("success.form.bv", function (e) {
		e.preventDefault();
		fuelformadd("#acce_from");
		//$("#registration-form").addClass("hidden");
		//$("#confirmation").removeClass("hidden");
	});
	

	
	
	
	
	
	
	
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	
	
	
	
	
	
	
	var validator = $("#insuarancefrom").bootstrapValidator({
		feedbackIcons: {
		
		}, 
		fields : {
			idate :{
				validators : {
					notEmpty : {
						message : "date is required"
					}
				}
			}, 
		
			vnum : {
				validators: {
					notEmpty : {
						message : "Vehicle is required"
					}
				}
			},
			tvalue : {
				validators: {
					notEmpty : {
						message : "Value is required"
					}
				}
			}
		}
	});

	validator.on("success.form.bv", function (e) {
		e.preventDefault();
		fuelformadd("#insuarancefrom");
		//$("#registration-form").addClass("hidden");
		//$("#confirmation").removeClass("hidden");
	});
	

	
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	
	
	
	
	
	
	
	var validator = $("#licencefrom").bootstrapValidator({
		feedbackIcons: {
		
		}, 
		fields : {
			idate :{
				validators : {
					notEmpty : {
						message : "Licence Date is required"
					}
				}
			}, 
		
			vnum : {
				validators: {
					notEmpty : {
						message : "Vehicle is required"
					}
				}
			},
			edate : {
				validators: {
					notEmpty : {
						message : "Expire Date required"
					}
				}
			},
			tvalue : {
				validators: {
					notEmpty : {
						message : "Value is required"
					}
				}
			}
		}
	});

	validator.on("success.form.bv", function (e) {
		e.preventDefault();
		fuelformadd("#licencefrom");
		//$("#registration-form").addClass("hidden");
		//$("#confirmation").removeClass("hidden");
	});
	
	
	
	
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	
	
	var validator = $("#accedentfrom").bootstrapValidator({
		feedbackIcons: {
		
		}, 
		fields : {
			idate :{
				validators : {
					notEmpty : {
						message : "Licence Date is required"
					}
				}
			}, 
		
			vnum : {
				validators: {
					notEmpty : {
						message : "Vehicle is required"
					}
				}
			},
			tvalue : {
				validators: {
					notEmpty : {
						message : "Value is required"
					}
				}
			}
		}
	});

	validator.on("success.form.bv", function (e) {
		e.preventDefault();
		fuelformadd("#accedentfrom");
		//$("#registration-form").addClass("hidden");
		//$("#confirmation").removeClass("hidden");
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
});