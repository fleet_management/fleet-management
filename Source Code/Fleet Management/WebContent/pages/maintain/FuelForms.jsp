<%@page import="com.roshan.entity.TblVehicle"%>
<%@page import="com.roshan.model.VehicleModel"%>
<%@page import="com.roshan.entity.TblFuleType"%>
<%@page import="java.util.List"%>
<%@page import="com.roshan.model.FuelModel"%>
<div>

<form class="form-horizontal" id="fuelfrom" >

 <%
 
 FuelModel fm = new FuelModel();
 List <TblFuleType> list = fm.getAllFuel();
 
 VehicleModel vm = new VehicleModel();
 List <TblVehicle> vlist = vm.getAllVehicle();
 
 
 %>
				
				<div class="form-group" id="form_static">
				    <label  class="col-sm-2 control-label">Date </label>
				    <div class="col-sm-2">
				    	<input type="date" class="form-control input-sm" id="idate" name="idate" required>
				    </div>
		
			  <input type="hidden"  id="id" name="id">
			   <input type="hidden"  id="cat" name="cat" value="fuel">
			
				    <label for="ftype" class="col-sm-1 control-label"> Fuel Type</label>
				    <div class="col-sm-2">
				    
					    <select class="form-control input-sm" id="ftype" name="ftype" onchange="chooseprice(this.value);"  required>   
					    	
					    	<option> Select Fuel Type</option>
					    	<%for(TblFuleType te : list){ %>
					    	<option value="<%= te.getId()%>"><%= te.getName()%></option>
					    	<%} %>
					    </select>
				    
				    </div>
			
			  
			  	
				    <label for="uprice" class="col-sm-1 control-label">Unit Price </label>
				    <div class="col-sm-1">
				   		 <input type="number" class="form-control input-sm" id="uprice" value="" name="uprice">
				    </div>
			  </div>
			  
			  			
				<div class="form-group" id="form_dynamic">
				    
				        <label for="vnum" class="col-sm-2 control-label"> Vehicle Number</label>
				    <div class="col-sm-2">
				    
					    <select class="form-control input-sm" id="vnum" name="vnum" required>   
					    <option> Select a Vehicle</option>
					     	<%for(TblVehicle te : vlist){ %>
					    	<option value="<%= te.getId()%>"><%= te.getRegistrationNumber()%></option>
					    	<%} %>
					    </select>
				    
				    </div>
				    
				    
				    <label for="nunit" class="col-sm-1 control-label">Num Of Units </label>
				    <div class="col-sm-1">
				    	<input type="text" class="form-control input-sm" id="nunit" name="nunit" onkeyup="calSubTotal(this.value);" required >
				    </div>
		
			  
			
				
			
			  
			  	
				    <label for="stotal" class="col-sm-1 control-label">Subtotal </label>
				    <div class="col-sm-1">
				   		 <input type="number" class="form-control input-sm" id="stotal" name="stotal" >
				    </div>
				    
				      <label for="discount" class="col-sm-1 control-label">Discount % </label>
				    <div class="col-sm-1">
				   		 <input type="text" class="form-control input-sm" id="discount" name="discount" onkeyup="caldiscount(this.value);" value="0">
				    </div>
				    
				  <label  class="col-sm-1 control-label" >Total Value </label>
				    <div class="col-sm-1">
				   		 <input type="text" class="form-control input-sm" id="tvalue" name="tvalue"  >
				    </div>
			
	

  
  	
 		
 		  </div>
 		  
 		  	 <div class="btn_save_clear">
 		
 		<button type="reset" class="btn btn-danger">reset</button>
  		<button type="submit" class="btn btn-success"  id="subm" >Save</button>
 		</div>
 		
  
  
</form>


</div>
	





