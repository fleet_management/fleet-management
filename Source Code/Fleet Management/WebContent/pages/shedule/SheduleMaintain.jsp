<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body onload=" readSheduleRecords();">
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="#">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>SHEDULE</h3>
  </div>
    
  

  
  <div style="clear:both"></div>
        <div id="shadde" class="alert alert-danger hidden">
          <span class="glyphicon glyphicon-star"></span> Shedule Record Alredy Exits
        </div>
       
         <div id="shadds" class="alert alert-success hidden">
          <span class="glyphicon glyphicon-star"></span> Shedule Record Added Sucssefully
        </div>
       
	
	

	
	
    
  </div>
</div>


<!-- --End Upper Panel -->
<!-- -Start Form -->

<div class="panel panel-default" id="internal_form">
  <div class="panel-body">
  
  	<jsp:include page="SheduleForms.jsp"/>
  
  </div>
  </div>

<!-- -End Form -->
<div class="panel panel-default" style="background-color:#eee;">
  <div class="panel-body" >
  
    <div class="row" id="form_static"  >
  <form id="selectsedule">
		  <div class="col-md-5">
		    	
						      <h6>Departure time </h6>
						   	 <input type="text" class="form-control input-sm" id="sdtime" name="sdtime" required>
		</div>
						    
			<div class="col-sm-5">
						   <h6>Arrival Time  </h6>
						   <input type="text" class="form-control input-sm" id="satime" name="satime" required>
				    
		  </div>
		<br><h6></h6>
		     <button type="button" class="btn btn-success " id="sbtn" name="sbtn" onclick ="selectSheduleRecords()">Search</button>
   
     </form>
  </div>
  
  
  </div>
  </div>

<div class="panel panel-default" ">
  <div class="panel-body" >
  
  
  
      <div class="row">

  		 <div class="col-md-6">
  		 	
					<div class="input-group" id="search_bar" >
                      <input type="text" name="message" placeholder="Search Here..." class="form-control" onkeyup="searchShedule(this.value);">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-flat">Search</button>
                      </span>
                   </div>

  		 </div>
  		             
	</div>

  

</div><!-- /.row -->
  


<div class="row tbl_border" >
<table id="example" class="table table-bordered table-hover tbl_cus dataTable" style="background-color:white;">
 

     


<thead>
           <tr>
           	<th class="col-xs-1">No</th>
             <th class="col-xs-2">Vehicle No</th>
             <th class="col-xs-2">Date</th>
              <th class="col-xs-2">From</th>
               <th class="col-xs-2">To</th>
             <th class="col-xs-3">Action</th>
           </tr>
</thead>

	<tbody id="shetable">
	
	
	</tbody>

		</table>
	</div>
	
	

	
	
	
	
  </div>


 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
