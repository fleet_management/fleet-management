<%@page import="com.roshan.model.EmployeeModel"%>
<%@page import="com.roshan.entity.TblEmployee"%>
<%@page import="com.roshan.entity.TblVehicle"%>
<%@page import="com.roshan.model.VehicleModel"%>
<%@page import="java.util.List"%>
<%@page import="com.roshan.model.AccessorieModel"%>


<div>

<form class="form-horizontal" id="shedulefrom" >

 <%
 
EmployeeModel am = new EmployeeModel();
 List <	TblEmployee> list = am.getAllEmployee();
 
 VehicleModel vm = new VehicleModel();
 List <TblVehicle> vlist = vm.getAllVehicle();
 
 
 %>
			  			
				<div class="form-group" id="form_static" >
				    
			
						    <label for="idate" class="col-sm-2 control-label">Vehicle Number </label>
				       <div class="col-sm-3">
				    
					    <select class="form-control input-sm" id="vnum" name="vnum">   
					   	<option> Select a Vehicle</option>
					     	<%for(TblVehicle te : vlist){ %>
					    	<option value="<%= te.getId()%>"><%= te.getRegistrationNumber()%></option>
					    	<%} %>
					    </select>
					  
				    
				    </div>
				    
			  
			
				    <label for="vnum" class="col-sm-2 control-label"> Driver Name</label>
				    <div class="col-sm-3">
				    
					    <select class="form-control input-sm" id="enum" name="enum">   
					   	<option> Select a Driver</option>
					     	<%for(TblEmployee te : list){ %>
					    	<option value="<%= te.getId()%>"><%= te.getName()%></option>
					    	<%} %>
					    </select>
					  
				    
				    </div>
				    
			
				     <input type="hidden" id="id" name="id">
			</div>    
				
				   <div class="form-group" > 
				   
	
						<div class="col-sm-3">
				      <h6>Departure time </h6>
				   		 <input type="text" class="form-control input-sm" id="dtime" name="dtime" >
				    </div>
				    
				    <div class="col-sm-3">
				      <h6>Arrival Time  </h6>
				   		 <input type="text" class="form-control input-sm" id="atime" name="atime" >
				    </div>
				    
				    <div class="col-sm-3">
				      <h6>From </h6>
				   		 <input type="text" class="form-control input-sm" id="from" name="from" >
				    </div>
			
				    <div class="col-sm-3">
				      <h6>To </h6>
				   		 <input type="text" class="form-control input-sm" id="to" name="to" >
				    </div>
			
				
					
  
  	
 		
 		  </div>
 		  
 		  <div class="form-group" >
 		  
 		  	    <div class="col-sm-12">
				    <h6> Discription </h6>
				    	 <textarea class="form-control input-sm"  rows="3" id="she_disc" name="she_disc"></textarea>
				    </div>
 		  
 		  </div>
 		  
 		  	 <div class="btn_save_clear">
 		
 		<button type="reset" class="btn btn-danger">reset</button>
  		<button type="submit" class="btn btn-success">Save</button>
 		</div>
 		
  
  
</form>


</div>
	





