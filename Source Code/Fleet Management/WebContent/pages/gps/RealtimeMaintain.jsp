<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body >
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="GpsMaintain.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>REALTIME</h3>
  </div>
    
  

	
    
  </div>
</div>


<!-- --End Upper Panel -->
<!-- -Start Form -->

<div class="panel panel-default" id="internal_form">
  <div class="panel-body">
  
  	<jsp:include page="RealtimeForms.jsp"/>
  
  </div>
  </div>

<!-- -End Form -->

<div class="panel panel-default">
  <div class="panel-body">



<script>
	function loadMap(lat ,lont) {
		//var lat = 7.2946588;
		//var lont = 80.6308816;
	var mapOptions = {
			center:new google.maps.LatLng(lat, lont),
			zoom:12,
			mapTypeId:google.maps.MapTypeId.ROADMAP
	};
		var map=new google.maps.Map(document.getElementById("sample"),mapOptions);

		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lont),
			map: map,
		});
	}
	
	//google.maps.event.addDomListener(window, 'load', loadMap);
</script>



		<div id="sample" style="width:auto;height:580px;">
		
		
		
		
		
		</div>

   
</div>


 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
