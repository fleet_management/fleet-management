<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body>
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="GpsMaintain.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>HISTORY</h3>
  </div>
    
  

	
    
  </div>
</div>


<!-- --End Upper Panel -->
<!-- -Start Form -->

<div class="panel panel-default" id="internal_form">
  <div class="panel-body">
  
  	<jsp:include page="HistoryForms.jsp"/>
  
  </div>
  </div>

<!-- -End Form -->

<div class="panel panel-default">
  <div class="panel-body">



<script>




$(document).ready(function() {
	   
	$('#sdate').datetimepicker({
		dateFormat: 'yy-mm-dd', 
        timeFormat: 'HH:mm' 
	});
	
	
	$('#edate').datetimepicker({
		dateFormat: 'yy-mm-dd', 
        timeFormat: 'HH:mm' 
	});
	

	
} );


	function loadMaparea(lapoint,lopoint  ,array) {


		var map = new google.maps.Map(document.getElementById('map'), {
	          zoom: 10,
	          center: {lat: parseFloat(lapoint), lng: parseFloat(lopoint)},
	          mapTypeId: 'terrain'
	        });

	        var flightPlanCoordinates = [
	         array
	        ];
	        var flightPath = new google.maps.Polyline({
	          path: flightPlanCoordinates,
	          geodesic: true,
	          strokeColor: '#FF0000',
	          strokeOpacity: 1.0,
	          strokeWeight: 2
	        });

	        flightPath.setMap(map);
	}
	
	//google.maps.event.addDomListener(window, 'load', loadMaparea);
</script>



		<div id="map" style="width:auto;height:580px;">
		
		
		
		
		
		</div>



   
</div>


 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
