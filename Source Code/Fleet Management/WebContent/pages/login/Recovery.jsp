<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body>
<div class="container">



<div class="panel panel-default" id="login_panel" style="width:70%">


  <div class="panel-body">
  
   <div style="clear:both"></div>
    <div class="alert alert-danger hidden" role="alert" id="loginval">
 	 <strong>Warning!</strong> User Name Or Password Incorrect
	</div>

<form class="form-horizontal"  id="resetForm" method="post" action="RecoveryPassword" >
	
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">User Registration Number</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="userName" name="userName" >
       <span class="help-block" id="userNameMessage" />
    </div>

    <div class="col-sm-2">
      <button type="submit" id="btn_signin" class="btn btn-primary ">Recovery Password</button>
    </div>
  </div>
</form>


		

  </div>
</div>


</div>
</body>
</html>