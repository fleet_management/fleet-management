<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body>
<div class="container">



<div class="panel panel-default" id="login_panel">
<div class="panel-heading" id="login_header">Fleet Management System</div>

  <div class="panel-body">
  
  <%
  
  try{
	  
	 
	int  q = Integer.parseInt(request.getParameter("q"));
	  if(q==1){
		  
		  %>
	   <div style="clear:both"></div>
	    <div class="alert alert-danger " role="alert" id="loginval">
	 	 <strong>Warning!</strong> User Name Or Password Incorrect
		</div>
	  <% 
	  }
  }catch(Exception e){}
  %>



  <%
  
  try{
	  
	 
	int  q = Integer.parseInt(request.getParameter("q"));
	  if(q==2){
		  
		  %>
	   <div style="clear:both"></div>
	    <div class="alert alert-danger " role="alert" id="loginval">
	 	 <strong>Warning!</strong> You do not have permission to access to Admin Account
		</div>
	  <% 
	  }
  }catch(Exception e){}
  %>
	
<form class="form-horizontal"  id="loginform" method="post" action="LoginForm" >
	
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">User Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="userName" name="userName" required >
       <span class="help-block" id="userNameMessage" />
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="password" name="password" required>
       <span class="help-block" id="passwordMessage" />
    </div>
  </div>

  <div class="form-group">
  
   <div class="col-sm-offset-3 col-sm-4">
      
        <label>
          <a href="Recovery.jsp"> Forgot Password ? </a>
        </label>
    
    </div>
    
    <div class=" col-sm-5">
      <button type="submit" id="btn_signin" class="btn btn-primary " style="width:100%">Sign in</button>
    </div>
  </div>
</form>


		

  </div>
</div>


</div>
</body>
</html>