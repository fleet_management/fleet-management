<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body >
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="setting.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>PASSWORD CHANGE</h3>
  </div>
    
  

  <div style="clear:both"></div>
        
       
         <div id="pwds" class="alert alert-success hidden">
          <span class="glyphicon glyphicon-star"></span>  Password Updated
        </div>
       
       
            <div id="pwde" class="alert alert-danger hidden">
          <span class="glyphicon glyphicon-star"></span> Old Password Do Not Match
        </div>
       
	
	

	
	
    
  </div>
</div>


<!-- --End Upper Panel -->


<div class="panel panel-default">
  <div class="panel-body">
  
  

		<form class="form-horizontal" id="adminform">
		
		
		  <div class="form-group">
		    <label class="col-sm-2 control-label">Current Password</label>
		    <div class="col-sm-7">
		      <input type="password" class="form-control" id="cpwd"  name="cpwd">
		    </div>
		  </div>
		  
		
		  <div class="form-group">
		    <label class="col-sm-2 control-label">New Password</label>
		    <div class="col-sm-7">
		      <input type="password" class="form-control" id="pwd1"  name="pwd1">
		    </div>
		  </div>
		  
		  
		    <div class="form-group">
		    <label class="col-sm-2 control-label">Retype Password</label>
		    <div class="col-sm-7">
		      <input type="password" class="form-control" id=pwd2"  name="pwd2">
		    </div>
		  </div>
		   <input type="text" class="form-control" id="id"  name="id" value="531092502">
		
		  <div class="form-group">
		    <div class="col-sm-offset-8 col-sm-9">
		      <button type="submit" class="btn btn-success btn-sm">Update</button>
		    </div>
		  </div>
		  
		</form>
		



   
</div>

  </div>


 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
