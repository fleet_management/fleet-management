$(document).ready(function () {
	
	
		var validator = $("#orgform").bootstrapValidator({
			feedbackIcons: {
				
			}, 
			fields : {
				name :{
					validators : {
						notEmpty : {
							message : "Organization Name is required"
						}
					}
				},
				address :{
					validators : {
						notEmpty : {
							message : "Organization Address is required"
						}
					}
				},
				tel :{
					validators : {
						notEmpty : {
							message : "Organization Telephone number is required"
						}
					}
				},
				fax :{
					validators : {
						notEmpty : {
							message : "Organization Fax is required"
						}
					}
				},
				email :{
					validators : {
						notEmpty : {
							message : "Organization Email is required"
						}
					}
				},
				web :{
					validators : {
						notEmpty : {
							message : "Organization Email is required"
						}
					}
				}
				
			}
		});
	
		validator.on("success.form.bv", function (e) {
			e.preventDefault();
			mainDelatisUpdate();
			
		});
		
		
		
		
		var validator = $("#adminform").bootstrapValidator({
			feedbackIcons: {
				
			}, 
			fields : {
				cpwd :{
					validators : {
						notEmpty : {
							message : "Current Password is required"
						}
					}
				},
				pwd1 :{
					validators : {
						notEmpty : {
							message : "Password is required"
						},
	                    identical: {
	                        field: 'pwd2',
	                        message: 'The password and its confirm are not the same'
	                    }
					}
				},
				pwd2 :{
					validators : {
						notEmpty : {
							message : "Password is required"
						},
	                    identical: {
	                        field: 'pwd1',
	                        message: 'The password and its confirm are not the same'
	                    }
					}
				}
			}
		});
	
		validator.on("success.form.bv", function (e) {
			
			changePassword();
			e.preventDefault();
			
			
		});
		





	
	
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
});