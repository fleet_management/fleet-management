<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body onload="MaindetailsView()">
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="setting.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>MAIN PROPERTY DETAILS</h3>
  </div>
    
  

  <div style="clear:both"></div>
        
       
         <div id="mde" class="alert alert-success hidden">
          <span class="glyphicon glyphicon-star"></span> Record Update Sucssefully
        </div>
       
	
	

	
	
    
  </div>
</div>


<!-- --End Upper Panel -->


<div class="panel panel-default">
  <div class="panel-body">
  
  

		<form class="form-horizontal" id="orgform">
		
		  <div class="form-group">
		    <label class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-7">
		      <input type="text" class="form-control" id="name"  name="name">
		    </div>
		  </div>
		  
		  <div class="form-group">
		    <label class="col-sm-2 control-label">Address</label>
		    <div class="col-sm-7">
		    
		    
		      <textarea class="form-control" rows="3" id="address" name="address">
		      
		      </textarea>
		    </div>
		  </div>
		  
		  
		    <div class="form-group">
		    <label class="col-sm-2 control-label">Telephone Number</label>
		    <div class="col-sm-7">
		      <input type="text" class="form-control" id="tel" name="tel">
		    </div>
		  </div>
		  
		    <div class="form-group">
		    <label  class="col-sm-2 control-label">Fax Number</label>
		    <div class="col-sm-7">
		      <input type="text" class="form-control" id="fax" name="fax">
		    </div>
		  </div>
		  
		    <div class="form-group">
		    <label  class="col-sm-2 control-label">Email Address</label>
		    <div class="col-sm-7">
		      <input type="email" class="form-control" id="email" name="email">
		    </div>
		  </div>
		  
		    <div class="form-group">
		    <label class="col-sm-2 control-label">Website</label>
		    <div class="col-sm-7">
		      <input type="text" class="form-control" id="web" name="web">
		    </div>
		  </div>
	
		
		  <div class="form-group">
		    <div class="col-sm-offset-8 col-sm-9">
		      <button type="submit" class="btn btn-success btn-sm">Update</button>
		    </div>
		  </div>
		  
		</form>
		



   
</div>

  </div>


 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
