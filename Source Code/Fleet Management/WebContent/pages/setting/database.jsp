<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body >
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="setting.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>DATABASE</h3>
  </div>
    
  

  <div style="clear:both"></div>
        
       
         <div id="mde" class="alert alert-success hidden">
          <span class="glyphicon glyphicon-star"></span>  Sucssefully
        </div>
       
	
	

	
	
    
  </div>
</div>


<!-- --End Upper Panel -->


<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">BACKUP DATABASES</h3>
  </div>
  <div class="panel-body">
  
  
  <button type="button" class="btn btn-success btn-sm" style="width: 100%;" onclick="backup()">BACKUP</button>
  
  </div>
</div>



<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">RESTORE DATABASE</h3>
  </div>
  <div class="panel-body">
   
   
   
		<form class="form-horizontal" id="restoreform">
		
		  <div class="form-group">
		    <label class="col-sm-2 control-label">Choose a Backup File</label>
		    <div class="col-sm-7">
		      <input type="file" class="form-control" id="bfile"  name=bfile">
		    </div>
		  </div>
		   
		
		  <div class="form-group">
		    <div class="col-sm-offset-8 col-sm-9">
		      <button type="submit" class="btn btn-success btn-sm">RESTORE</button>
		    </div>
		  </div>
		  
		</form>
   
   
   
  </div>
</div>


 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
