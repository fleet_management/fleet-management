<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body>
<div class="container" >
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content" >
										
<!--- Container-------->

    
    <div class="panel panel-default panel_custom">
  <div class="panel-body">

  	 <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="#">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>REPORT</h3>
  </div>

  </div>
  </div>




  <div class="row">



    <div class="col-md-4">
      <div class="panel panel-default text-center">
        <div class="panel-heading"style="background: #5cb85c; color: #fff;">
          <h4>VEHICLE</h4>
        </div>
        <div class="panel-body">
         Vehicle Report
        </div>
        <div class="panel-footer" style="background: #fff; border-top: 0px;">
           <a href="AccessoryMaintain.jsp"><button type="button" class="btn btn-default btn-sm">Click Here</button></a>
        </div>
      </div>
    </div>






    <div class="col-md-4">
      <div class="panel panel-default text-center">
        <div class="panel-heading"style="background: #18bc9c; color: #fff;">
          <h4>INDIVIDUAL</h4>
        </div>
        <div class="panel-body">
        Individual Report
        </div>
        <div class="panel-footer" style="background: #fff; border-top: 0px;">
           <a href="AccessoryMaintain.jsp"><button type="button" class="btn btn-default btn-sm">Click Here</button></a>
        </div>
      </div>
    </div>
    
    
        <div class="col-md-4">
      <div class="panel panel-default text-center">
        <div class="panel-heading" style="background: #f0ad4e; color: #fff;">
          <h4>SUMMERY</h4>
        </div>
        <div class="panel-body">
           Summery Report
        </div>
        <div class="panel-footer" style="background: #fff; border-top: 0px;">
           <a href="AccessoryReport.jsp"><button type="button" class="btn btn-default btn-sm">Click Here</button></a>
        </div>
      </div>
    </div>
    </div>
    
    











 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
