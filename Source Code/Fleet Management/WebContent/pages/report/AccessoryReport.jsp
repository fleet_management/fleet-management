<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body >
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="report.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>SUMMERY REPORT</h3>
  </div>
    
  

   <div style="clear:both"></div>
        
	

	
	
    
  </div>
</div>


<!-- --End Upper Panel -->
<!-- -Start Form -->

<div class="panel panel-default" id="internal_form">
  <div class="panel-body">
  
  	<jsp:include page="AccessoryReportForms.jsp"/>
  
  </div>
  </div>

<!-- -End Form -->






<!-- --------------Second Table -->


<div class="panel panel-default">
  <div class="panel-body">


   
</div>
<div class="row tbl_border">
<table class="table table-bordered table-hover tbl_cus">
 

     


<thead>
           <tr>
           	<th class="col-xs-1">No</th>
             <th class="col-xs-2">Maintance</th>
             <th class="col-xs-2">Num Of Maintaince</th>
             <th class="col-xs-3">Total Rs</th>
           </tr>
</thead>

		<tbody id="accsum">
		
		
		
		</tbody>

		</table>
	</div>
	

	
	
	
	
  </div>
  
  
  
  
  
  
  
  
  




 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
