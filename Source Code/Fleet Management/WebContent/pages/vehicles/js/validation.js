$(document).ready(function () {
		var validator = $("#addvehicle").bootstrapValidator({
			
			feedbackIcons: {
				valid: "glyphicon glyphicon-ok",
				invalid: "glyphicon glyphicon-remove", 
				validating: "glyphicon glyphicon-refresh"
			}, 
			fields : {
				vehicle_num :{
					validators : {
						notEmpty : {
							message : "Vehicle Number is required"
						}
					}
				}, 
				model : {
					validators: {
						notEmpty : {
							message : "Model is required"
						}
					}
				}, 
				class_type : {
					validators: {
						notEmpty : {
							message: "Class Type is required"
						}
					}
				}, 
				engine : {
					validators: {
						notEmpty : {
							message: "Engine Number is required"
						}
					}
				},	
				chasi : {
					validators: {
						notEmpty : {
							message: "Chasi Number is required"
						}
					}
				},
				licece : {
					validators: {
						notEmpty : {
							message: "Licence Number is required"
						}
					}
				},
				expire_date : {
					validators: {
						notEmpty : {
							message: "Expire date is required"
						}
					}
				},
				def_per : {
					validators: {
						notEmpty : {
							message: "Depreciation Per Year is required"
						}
					}
				}
			}
		});
	
		validator.on("success.form.bv", function (e) {
			e.preventDefault();
			Vehicleadd();
			//$("#registration-form").addClass("hidden");
			//$("#confirmation").removeClass("hidden");
		});
		
	});