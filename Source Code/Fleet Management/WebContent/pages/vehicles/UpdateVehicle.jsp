<%@page import="com.roshan.model.VehicleModel"%>
<%@page import="com.roshan.entity.TblVehicle"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


  <%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
  
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body onload=" getVehicle(<%= Integer.parseInt(request.getParameter("q"))%>)">
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->



<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="Vehicle.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>UPDATE VEHICLE</h3>
  </div>
    
  
  

  
  

  <div style="clear:both"></div>
   

			
      <div id="vadde" class="alert alert-danger hidden">
          <span class="glyphicon glyphicon-star"></span> Vehicle Alredy Exits
        </div>
       
         <div id="vadds" class="alert alert-success hidden">
          <span class="glyphicon glyphicon-star"></span> Vehicle Added Sucssefully
        </div>
       


    
  </div>
</div>


<!-- --End Upper Panel -->

	<form class="form-horizontal" id="addvehicle">
<div id="vehicle_leftpanel">

<div class="panel panel-default">
  <div class="panel-heading">Basic Information</div>
  <div class="panel-body">

  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span class="acsil">*</span> Registration Number </label>
    <div class="col-sm-9">
    <input type="text" class="form-control input-sm" id="vehicle_num" name="vehicle_num"  >
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Model</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="model" name="model" >
    </div>
  </div>
 
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span>*</span> Class Type </label>
    <div class="col-sm-9">
    
 <input type="text" class="form-control input-sm" id="class_type" name="class_type" >
     
    </div>
  </div>
  
  <input type="hidden" id="id" name="id">
  
    <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span>*</span> Catogory </label>
    <div class="col-sm-8">
      <select class="form-control input-sm" id="catogory" name="catogory">
      
      <option value="cat 1"> Cat 1 </option>
      <option value="cat 2"> Cat 2 </option>
      
      </select>
    </div>
  </div>
  
  
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Capacity</label>
    <div class="col-sm-9">
     <input type="text" class="form-control input-sm" id="capacity" name="capacity">
    </div>
  </div>


  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span>*</span>Fuel Type </label>
    <div class="col-sm-8">
        
      <select class="form-control input-sm" id="fuel" name="fuel">
      
      <option value="cat 1"> Cat 1 </option>
      <option value=" cat2"> Cat 2 </option>
      
      </select>
    </div>
  </div>
    
  
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Engine Number</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="engine" name="engine">
    </div>
  </div>
   
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span>*</span> Chasi Number </label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="chasi" name="chasi" >
    </div>
  </div>
  
  
  
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Licence Number</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="licece" name="licece" >
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Licence Expire Date</label>
    <div class="col-sm-8">
      
      
        <input type="date" class="form-control input-sm" id="expire_date" name="expire_date" >
    </div>
  </div>
    <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Insuarance Company</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="i_company" name="i_company" >
    </div>
  </div>




  </div>
</div>






<div class="panel panel-default">
  <div class="panel-heading">Other Imformation</div>
  <div class="panel-body">
 






  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Year Manufacture</label>
    <div class="col-sm-8">
    <input type="text" class="form-control input-sm" id="year_manufac" name="year_manufac" >
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Date Import</label>
    <div class="col-sm-8">
       <input type="date" class="form-control input-sm" id="date_import" name="date_import" >
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Date Registration</label>
    <div class="col-sm-8">
       <input type="date" class="form-control input-sm" id="date_regis" name="date_regis" >
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Depreciation Per Year</label>
    <div class="col-sm-6">
      <input type="text" class="form-control input-sm" id="dep_per" name="dep_per">
    </div>
  </div>
    <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Purchased Price</label>
    <div class="col-sm-5">
      <input type="text" class="form-control input-sm" id="price" name="price">
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Number Of wheels</label>
    <div class="col-sm-5">
      <input type="text" class="form-control input-sm" id="wheels" name="wheels">
    </div>
  </div>
    <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Height</label>
    <div class="col-sm-5">
      <input type="text" class="form-control input-sm" id="height" name="height">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Width</label>
    <div class="col-sm-5">
      <input type="text" class="form-control input-sm" id="width" name="width">
    </div>
  </div>
    <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Length</label>
    <div class="col-sm-5">
      <input type="text" class="form-control input-sm" id="length" name="length">
    </div>
   </div>

  </div>
</div>

</div>

<div id="vehicle_rightpanel">

<div class="panel panel-default">
  <div class="panel-heading">Additional Information</div>
  <div class="panel-body">
  		
  <div id="image_div">
  
    <img src="../../resources/icons/v.png"  class="img-circle " id ="img_preview" style="width:200px;height:200px;">		
  		
  <div class="form-group" >
    <label for="inputPassword3" class="col-sm-3 control-label" >Photo</label>
    <div class="col-sm-7">
      <input type="file" class="form-control input-sm" id="photo" name="photo" >
    </div>
  </div>	

  </div>		
		
  		<div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Documents</label>
    <div class="col-sm-8">
      <input type="file" class="form-control input-sm" id="document" name="document">
    </div>
  </div>
  		
		
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-body">
  	
 		<div class="btn_save_clear">
 		
 		<button type="reset" class="btn btn-warning">Clear</button>
  		<button type="submit" class="btn btn-success">Save</button>
 		</div>
		
  </div>
</div>
</div>
</form>
<div id="break_line"></div>



  <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
