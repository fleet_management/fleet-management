
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.roshan.entity.TblSchedule"%>
<%@page import="com.roshan.model.SheduleModel"%>
<%@page import="com.roshan.model.EmployeeModel"%>
<%@page import="com.roshan.entity.TblEmployee"%>
<%@page import="com.roshan.entity.TblVehicle"%>
<%@page import="com.roshan.model.VehicleModel"%>
<%@page import="java.util.List"%>
<%@page import="com.roshan.model.AccessorieModel"%>



<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>

<!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body>
<div class="container">
	
		<jsp:include page="../common/top_panel.jsp"/>

	<div class="wrapper clearfix">
		
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
					
					
<!--------------------------- Container --------------------------->

 <%
 
EmployeeModel am = new EmployeeModel();
 List <	TblEmployee> list = am.getAllEmployee();
 
 VehicleModel vm = new VehicleModel();
 List <TblVehicle> vlist = vm.getAllVehicle();
 
 
 Calendar calendar = Calendar.getInstance();
 calendar.add(Calendar.DAY_OF_YEAR, 1);
 
 // now get "tomorrow"
 Date tomorrow = calendar.getTime();
 
 SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm",Locale.ENGLISH);
 
 SheduleModel sm = new SheduleModel();
 Date date = new Date();
 List <TblSchedule>  slist = sm.SelectSedule(formatter.parse(formatter.format(tomorrow)), formatter.parse(formatter.format(date)));
 
 %>
 
 
 


<div class="panel panel-default ">
  <div class="panel-body dash_center">

  	<div class="row">



            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">

                     <h3><%= vlist.size() %>\<%= slist.size() %></h3>
                  <p>Vehicles</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
			
			
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
   				 <h3><%= list.size() %>\<%= slist.size() %></h3>
                  <p>Drivers</p>
                </div>
                <div class="icon">
                  <i class="fa  fa-user"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
			
			
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              
              
              <div class="small-box bg-yellow">
                <div class="inner">

                <h3> 0</h3>
              <p>Massages</p>
                </div>
               <a href="#"> <div class="icon">
                  <i class="fa fa-envelope-o"></i>
                </div></a>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
			
			
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>0</h3>
                  <p>Warning</p>
                </div>
                <div class="icon">
                  <i class="fa fa-warning"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
			
    </div>
  	
  	
  		

</div>
</div>



<!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>



