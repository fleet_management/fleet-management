<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body>
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->




<!-- - Start Upper Panel---->

<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="Employee.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>VENDER MANAGE</h3>
  </div>
    
  

  <div style="clear:both"></div>
    <div class="alert alert-danger " role="alert" id="empo">
 	 <strong>Warning!</strong> User Registation Number Is Already Exits
	</div>
    
  </div>
</div>


<!-- --End Upper Panel -->

<div class="panel panel-default">
  <div class="panel-body">

  <div class="row">

  		 <div class="col-md-6">
  		 	
					<div class="input-group" id="search_bar" >
                      <input type="text" name="message" placeholder="Search Here..." class="form-control">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-flat">Search</button>
                      </span>
                   </div>

  		 </div>
  		 <div class="col-md-4">

  		 </div>
  <div class="col-md-2">
  		<a  href="create.php" class="btn btn-success " data-toggle="modal" data-target=".add_new">New Vender</a> 


<div class="modal fade add_new" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
      <div class="modal-body">
       
       
       
       
       
       


<div class="panel panel-default">
  <div class="panel-heading">New Vender</div>
  <div class="panel-body">
       
       		<form class="form-horizontal">
       	
       	  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span class="acsil">*</span>Name</label>
    <div class="col-sm-8">
    <input type="text" class="form-control input-sm" id="vender_name" name="vender_name">
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Address</label>
    <div class="col-sm-8">
     <textarea class="form-control input-sm"  rows="3" id="address" name="address" ></textarea>
    </div>
  </div>
 
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span>*</span> Telephone</label>
    <div class="col-sm-7">
    
 <input type="text" class="form-control input-sm" id="telephone" name="telephone">
     
    </div>
  </div>
  
    <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span>*</span> Hot Line</label>
    <div class="col-sm-7">
    
 <input type="text" class="form-control input-sm" id="hotline" name="hotline">
     
    </div>
  </div>
  
  
    <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span>*</span>Fax </label>
    <div class="col-sm-7">
    <input type="text" class="form-control input-sm" id="fax" name="fax">
    </div>
  </div>
  
      <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label"><span>*</span>Web </label>
    <div class="col-sm-7">
    <input type="text" class="form-control input-sm" id="web" name="web">
    </div>
  </div>
       
       
       <div class="btn_save_clear">
 		
 		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
  		<button type="button" class="btn btn-success">Save</button>
 		</div>
 		</form>
       

</div>
</div>







       
       
       
       
       
       
       
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




  </div>                 
	</div>

   
</div>
<div class="row tbl_border">
<table class="table table-bordered table-hover">
 

     


<thead>
           <tr>
           	<th class="col-xs-1">Number</th>
             <th class="col-xs-3">Name</th>
             <th class="col-xs-3">Email Address</th>
             <th class="col-xs-2">Mobile Number</th>
             <th class="col-xs-3">Action</th>
           </tr>
</thead>

		<tr>			
			<td >1</td>		
			<td>Manjula</td>
			<td>dmrmanjula@yahpp.com</td>
			<td>0716227748</td>
			<td>      
				<a href="create.php" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-exclamation-sign cus" aria-hidden="true"></span>Read</a>
				<a href="create.php" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-exclamation-sign cus" aria-hidden="true"></span>Update</a>
				<a href="create.php" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove cus" aria-hidden="true"></span>Delete</a>

			 </td>
		</tr>
				<tr>			
			<td>1</td>		
			<td>Manjula</td>
			<td>dmrmanjula@yahpp.com</td>
			<td>0716227748</td>
			<td>      
				<a href="create.php" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-exclamation-sign cus" aria-hidden="true"></span>Read</a>
				<a href="create.php" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-exclamation-sign cus" aria-hidden="true"></span>Update</a>
				<a href="create.php" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove cus" aria-hidden="true"></span>Delete</a>

			 </td>
		</tr>

		</table>
	</div>
  </div>


 <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
