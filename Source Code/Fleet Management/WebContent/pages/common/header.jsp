<link rel="stylesheet" type="text/css" href="../../resources/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../resources/css/custom.css">
 <link href="../../resources/font/css/font-awesome.min.css" rel="stylesheet" type="text/css">
 <link rel="stylesheet" type="text/css" href="../../resources/css/formValidation.css">
 <link rel="stylesheet" type="text/css" href="../../resources/css/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="../../resources/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../../resources/css/jquery.datetimepicker.css"> 


<script type="text/javascript" src="../../resources/js/jQuery-2.1.4.min.js"></script>
<script type="text/javascript" src="../../resources/js/bootstrap.js"></script>
<script type="text/javascript" src="../../resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="../../resources/js/formValidation.js"></script>
<script type="text/javascript" src="../../resources/js/framework/bootstrap.js"></script>
<script type="text/javascript" src="../../resources/js/jquery.datetimepicker.full.js"></script>

<script type="text/javascript" src="../../resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../resources/js/dataTables.bootstrap.min.js"></script>


<script type="text/javascript" src="../../resources/js/employee/nullCheck.js"></script>
<script type="text/javascript" src="../../resources/js/employee/imagePreview.js"></script>

<script type="text/javascript" src="../login/js/validation.js"></script>
<script type="text/javascript" src="../login/js/ajax.js"></script>


<script type="text/javascript" src="../employee/js/validate.js"></script>
<script type="text/javascript" src="../employee/js/ajax.js"></script>

<script type="text/javascript" src="../vehicles/js/validation.js"></script>
<script type="text/javascript" src="../vehicles/js/ajax.js"></script>



<script type="text/javascript" src="../maintain/js/validation.js"></script>
<script type="text/javascript" src="../maintain/js/ajax.js"></script>

<script type="text/javascript" src="../shedule/js/validation.js"></script>
<script type="text/javascript" src="../shedule/js/ajax.js"></script>

<script type="text/javascript" src="../report/js/validation.js"></script>
<script type="text/javascript" src="../report/js/ajax.js"></script>


<script type="text/javascript" src="../setting/js/validation.js"></script>
<script type="text/javascript" src="../setting/js/ajax.js"></script>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAaUW_1p4axcl6h7WmCzjMMuHxoaovDLjk"></script>