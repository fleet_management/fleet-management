<%@page import="com.roshan.model.EmployeeModel"%>
<%@page import="com.roshan.entity.TblEmployee"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%>
    
    <!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body onload="searchEmployee('')">
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->
    
    
    
       
    <div class="panel panel-default panel_custom">
  <div class="panel-body">

  	 <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="#">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>EMPLOYEE</h3>
  </div>

  </div>
  </div>

<div class="panel panel-default">
  <div class="panel-body">

  <div class="row">

  		 <div class="col-md-6">
  		 	
					<div class="input-group" id="search_bar" >
                      <input type="text" name="message" placeholder="Search Here..." class="form-control" onkeyup="searchEmployee(this.value)">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-flat" id ="search_btn">Search</button>
                      </span>
                   </div>

  		 </div>
  		 <div class="col-md-4">

  		 </div>
  <div class="col-md-2">
  		<a  href="NewEmployee.jsp" class="btn btn-success "  data-target=".add_new">New Employee</a> 







  </div>                 
	</div>


   
</div>
<div class="row tbl_border">
<table class="table table-bordered table-hover">
 

     


<thead>
           <tr>
           	<th class="col-xs-1">Number</th>
             <th class="col-xs-3">Name</th>
             <th class="col-xs-3">Permenet Address</th>
             <th class="col-xs-2">Mobile Number</th>
             <th class="col-xs-3">Action</th>
           </tr>
</thead>

<tbody id="employee_content">


</tbody>

	
		</table>
		</div>
		
		
		
		
		
  </div>
  
  
  
  
  
  
  <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>

  