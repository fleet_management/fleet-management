 <%

if(session.getAttribute("id") == null || session.getAttribute("id").equals("")){
	
	response.sendRedirect("../login/loginform.jsp"); 
	
	
}

%> 
<%@page import="com.roshan.controller.*"%>
<!DOCTYPE html>
<html>
<head>
   <jsp:include page="../common/header.jsp"/> 
</head>
<body>
<div class="container">
	
	<jsp:include page="../common/top_panel.jsp"/>
	<div class="wrapper clearfix">
		<jsp:include page="../common/Menu.jsp"/>
		<div id="right_panel">
			<div id="content">	
					<div id="inside_content">
										
<!--- Container-------->
    


<div class="panel panel-default panel_heding">
  <div class="panel-body">
  
    <div class="back_btn"> 
   <nav>
 	  <ul class="pager" >
   		 <li><a href="Employee.jsp">&larr; Back</a></li> 
	  </ul>
 	</nav>
  </div>
  
  <div class="panel_name" >
  	<h3>ADD NEW EMPLOYEE</h3>
  </div>
    
  <div style="clear:both"></div>
    <div class="alert alert-danger hidden" role="alert" id="empo_exit">
 	 <strong>Warning!</strong> User Name or Password Incorrect
	</div>
    
    
     <div class="alert alert-success hidden" role="alert" id="empo_added">
 	 User Added Sucssefully
	</div>
    
    
  </div>
</div>

 






<form class="form-horizontal" id="newemployee" method="get" action="Employee" >
<div id="vehicle_leftpanel">

<div class="panel panel-default">
  <div class="panel-heading">Basic Information</div>
  <div class="panel-body">
   

  	
  	
  <div class="form-group">
    <label  class="col-sm-3 control-label"><span class="acsil">*</span> Name </label>
    <div class="col-sm-9">
     <textarea class="form-control input-sm"  rows="2" id="employee_name" name="employee_name" ></textarea>
     <span class="help-block" id="NameMessage" />
    </div>
  </div>
  
  <div class="form-group">
    <label  class="col-sm-3 control-label">Registration Number</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="employee_num" name="employee_num" value="">
      <span class="help-block" id="idMessage" />
    </div>
  </div>
 
  <div class="form-group">
    <label  class="col-sm-3 control-label"><span>*</span> Permenet Address </label>
    <div class="col-sm-9">
    
    <textarea class="form-control input-sm"  rows="4" id="permenet_address" name="permenet_address"></textarea>
    <span class="help-block" id="addressMessage" />
     
    </div>
  </div>
  <div class="form-group">
    <label  class="col-sm-3 control-label">Tempory Address</label>
    <div class="col-sm-9">
      <textarea class="form-control input-sm"  rows="4" id="tempory_address" name="tempory_address"></textarea>
    </div>
  </div>


  <div class="form-group">
    <label  class="col-sm-3 control-label"><span>*</span>NIC </label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="nic" name="nic">
      <span class="help-block" id="nicMessage" />
    </div>
  </div>
  <div class="form-group">
    <label  class="col-sm-3 control-label">Telephone</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="telephone" name="telephone">
      <span class="help-block" id="telMessage" />
    </div>
  </div>
   
  <div class="form-group">
    <label  class="col-sm-3 control-label"><span>*</span> Mobile Number </label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="mobile1" name="mobile1" >
      <span class="help-block" id="mobiMessage" />
    </div>
  </div>
  
    <div class="form-group">
    <label  class="col-sm-3 control-label"><span>*</span> Email </label>
    <div class="col-sm-9">
      <input type="email" class="form-control input-sm" id="mobile2" name="mobile2">
    </div>
  </div>
  
  <div class="form-group">
    <label  class="col-sm-3 control-label">Birthday</label>
    <div class="col-sm-9">
      <input type="date" class="form-control input-sm" id="bday" name="bday" >
      <span class="help-block" id="bdayMessage" />
    </div>
  </div>
  
  <div class="form-group">
    <label  class="col-sm-3 control-label">Gender</label>
    <div class="col-sm-9">
      
      
      <select class="form-control input-sm" id="gende" name="gende">
      <option value="male">Male</option>
      <option value="femail">Female</option>
      
      </select>
      <span class="help-block" id="genderMessage" />
    </div>
  </div>
  
  
  
    <div class="form-group">
    <label  class="col-sm-3 control-label">Role</label>
    <div class="col-sm-9">
      
      
      <select class="form-control input-sm" id="role" name="role">
      <option value="super_admin">Super Admin</option>
       <option value="admin">Admin</option>
        <option value="supervisor">Supervisor</option>
      <option value="employee">Employee</option>
      
      </select>
      <span class="help-block" id="genderMessage" />
    </div>
  </div>
  
  <input type="hidden" id="id" name="id">
    <div class="form-group">
    <label  class="col-sm-3 control-label">Fax</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="fax" name="fax" >
    </div>
  </div>

 <div class="form-group">
    <label  class="col-sm-3 control-label">Web</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="web" name="web" >
    </div>
  </div>




  </div>
</div>

</div>


<div id="vehicle_rightpanel">

<div class="panel panel-default">
  <div class="panel-heading">Other Imformation</div>
  <div class="panel-body">
 




  <div class="form-group">
    <label  class="col-sm-3 control-label">Licence Number</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="licence_number" name="licence_number">
    </div>
  </div>
  <div class="form-group">
    <label  class="col-sm-3 control-label">Licence Expired Date</label>
    <div class="col-sm-9">
      <input type="date" class="form-control input-sm" id="licence_ex_date" name="licence_ex_date" >
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">EPF Number</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="epf_number" name="epf_number" >
    </div>
  </div>
  <div class="form-group">
    <label  class="col-sm-3 control-label">Basic Salary</label>
    <div class="col-sm-9">
      <input type="text" class="form-control input-sm" id="salary" name="salary">
      
    </div>
  </div>






  </div>
</div>










<div class="panel panel-default">
  <div class="panel-heading">Additional Information</div>
  <div class="panel-body">
  	
  <div id="image_div">
  
  
  
  
 
  
    <img src="../../resources/icons/user.png"  class="img-circle " id ="img_preview">		
  		
  <div class="form-group" >
    <label for="inputPassword3" class="col-sm-3 control-label" >Photo</label>
    <div class="col-sm-7">
      <input type="file" class="form-control input-sm" id="photo" name="photo"  accept="image/*" >
    </div>
  </div>	
  
  
  
  </div>		
  		
	
  		
  		<div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">Documents</label>
    <div class="col-sm-8">
      <input type="file" class="form-control input-sm" id="document" name="document">
    </div>
  </div>
  		
  		
  		
  </div>
</div>




<div class="panel panel-default">
 
  <div class="panel-body">
  		
 
 		<div class="btn_save_clear">
 		
 		<button type="reset" class="btn btn-warning"  >Clear</button>
  		<button type="submit" class="btn btn-success" id ="employee_submit"  >Save</button>
 		</div>

   		
  </div>
</div>

</div>

</form>
<div id="break_line"></div>




  <!--------footer------------>
  
  </div>
			</div>
		<jsp:include page="../common/footer.jsp"/>	
	</div>
	</div>
		
</div>
</body>
</html>
