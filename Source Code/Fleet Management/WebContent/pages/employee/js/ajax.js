
function employeeAdd(){
	
	
	
	urlm="UpdateEmployee";
	if($("#id").val()=="" ){
		urlm = "Employee";
	}
	
		$.ajax({
			url:urlm,
			type:'POST',
			dataType:'json',
			enctype: 'multipart/form-data',
			data: $("#newemployee").serialize(),
			success: function(data){
				
				if(!data.isValid){
					$("#empo_exit").addClass("hidden");
					$("#empo_added").removeClass("hidden");
					$("#newemployee").data('formValidation').resetForm();
									
				}else{
					$("#empo_exit").removeClass("hidden");;
					$("#empo_added").addClass("hidden");
					$("#newemployee").data('formValidation').resetForm();
					
				}
				
			}
			
			
		});
		
		
}

function searchEmployee(word){
	 if(word==''){
	        word="NULL";
	    }
	   // alert(word);
	    $.post("EmployeeSearch", {
	            word:word

	    }, function (data) {
	        $("#employee_content").html(data);
	    });
	
}	
	

function deleteEmployee(id){
	
	
	$.post("DeleteEmployee", {
	        id:id
	
	}, function (data) {
		searchEmployee("");
	    
	});

}

function getEmployee(id){
	
	$.post("GetEmployee", {
        id:id

	}, function (data) {

	   $("#employee_name").val(data.employee_name);
	   $("#id").val(id);
	   $("#employee_num").val(data.employee_num);
	   $("#permenet_address").val(data.permenet_address);
	   $("#tempory_address").val(data.tempory_address);
	   $("#nic").val(data.nic);
	   
	   $("#telephone").val(data.telephone);
	   $("#mobile1").val(data.mobile1);
	   $("#mobile2").val(data.mobile2);
	   $("#bday").val(data.bday);
	   
	   $("#gende").val(data.gender);
	   $("#fax").val(data.fax);
	   $("#web").val(data.web);
	   
	   $("#licence_number").val(data.licence_number);
	   $("#licence_ex_date").val(data.licence_ex_date);
	   $("#epf_number").val(data.epf_number);
	   $("#salary").val(data.salary);
	   
	   $("#role").val(data.role);

	
   });
	
}