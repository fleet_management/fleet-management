
$(document).ready(function(){
	
	// ----------------------------login panel----------------------------//
	
	    $('#loginform').formValidation({
	        message: 'This value is not valid',
	      
	        fields: {
	            userName: {
	                err: '#userNameMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The User name is required'
	                    }
	                }
	            },
	            password: {
	                err: '#passwordMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The Password is required'
	                    }
	                }
	            }
	      
	        }
	    });

	

	
	//----------------------------------new Employee----------------------------------------------------
	    
	    
	    
	    
	    
	    $('#newemployee').formValidation({
	        message: 'This value is not valid',
	      
	        icon: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	        	employee_num: {
	                err: '#idMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The Employee Number is required '
	                    }
	                }
	            },
	            permenet_address: {
	                err: '#addressMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The Address is required '
	                    }
	                }
	            },
	            nic: {
	                err: '#nicMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The NIC is required '
	                    }
				            ,
			                stringLength: {
			                    min: 10,
			                    max: 11,
			                    message: 'Invalid NIC number'
			                }
	                }
	            },
	            telephone: {
	                err: '#telMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The Telephone Number is required '
	                    }
				            ,
			                stringLength: {
			                    min: 10,
			                    max: 15,
			                    message: 'Invalid Telephone Number'
			                		}
	                }
	            },
	            mobile1: {
	                err: '#mobiMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The Mobile Number is required '
	                    },
	                    
		                    stringLength: {
		                        min: 10,
		                        max: 15,
		                        message: 'Invalid Mobile Number'
		                    }
	                }
	            },
	            bday: {
	                err: '#bdayMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The Birthday is required '
	                    }
	                }
	            },
	            gender: {
	                err: '#genderMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The Gender is required '
	                    }
	                }
	            },
	            employee_name: {
	                err: '#NameMessage',
	                validators: {
	                    notEmpty: {
	                        message: 'The Employee Name is required '
	                    }
	                }
	            }
	            
	            
	            
	            
	      
	        }
	    })
		.on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var $form = $(e.target),
                fv    = $form.data('formValidation');

            employeeAdd();


        });
	    
	    
	    
	    
	

});



